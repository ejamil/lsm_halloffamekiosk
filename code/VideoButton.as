﻿package code
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.ProgressEvent;
	import flash.events.NetStatusEvent;
	import flash.net.URLRequest;
	import flash.net.URLLoader;
	import flash.net.NetConnection;
	import flash.net.NetStream;
	import flash.media.Video;
	import flash.filters.*;
	import flash.geom.Point;
	import flash.geom.Rectangle;

	import gs.*;
	import gs.easing.*;
	import code.UpperSearchBG;

	public class VideoButton extends MovieClip
	{
		private var menuVideo:Array = new Array();
		private var thumbArray:Array = new Array();
		private var captionArray:Array = new Array();
		private var copyrightArray:Array = new Array();
		public var next_btn:NextButton;
		public var prev_btn:PrevButton;
		private var yButtonLoc:Number = 102;
		private var videoHolder:Sprite = new Sprite; //add large video in to the Sprite
		private var thumbHolder:Sprite = new Sprite; //add thum images in to this Sprite
		public var thumbContainer:Sprite = new Sprite; //add thumbHolder images in to the container
		private var noofVid:int; //Number of videos 
		private var space:int = 100;
		private var owner:UpperSearchBG;
		private var boxWidth:Number;
		private var boxHeight:Number;
		public var currentIndex:Number;
		private var glowArray:Array = [new GlowFilter()];

		private var controls:VidControls;
		private var videoPath:String;
		private var vid:Video;
		private var nc:NetConnection = new NetConnection();
		private var ns:NetStream;
		private var netClient:Object;
		private var videoSized:Boolean;
		private var videoToggle:Boolean;
		private var isLoading:Boolean;
		private var preloader:Preloader;

		public function VideoButton(_menuVideo:Array, _captionsArray:Array, _copyrightArray:Array, needsButtons:Boolean, myOwner:UpperSearchBG):void
		{
			//////Reference to the parent class//////
			owner = myOwner;

			//////Set glow filter properties////// 
			glowArray[0].color = 0xFFFF00;
			glowArray[0].blurX = 15;
			glowArray[0].blurY = 15;

			//////Adds next and previous buttons and sets their properties//////
			if (needsButtons = true)
			{
				prev_btn = new PrevButton();
				this.addChild(prev_btn);
				prev_btn.x = 120;
				prev_btn.y = yButtonLoc;
				prev_btn.addEventListener(MouseEvent.CLICK,goBackward);

				next_btn = new NextButton();
				this.addChild(next_btn);
				next_btn.x = this.width - 75;
				next_btn.y = yButtonLoc;
				next_btn.addEventListener(MouseEvent.CLICK,goForward);
			}
			prev_btn.visible = false;
			next_btn.visible = false;

			//////Set max width and height of the top Video//////
			boxWidth = owner.upperInfo2.width;
			boxHeight = owner.upperInfo2.height;

			//////Populate Arrays//////
			menuVideo = _menuVideo;
			captionArray = _captionsArray;
			copyrightArray = _copyrightArray;
			
			trace("menuvideo = " + menuVideo[0]);

			noofVid = menuVideo.length;//get number of videos
			thumbContainer.mask = thumbMask;//set mask on thumbnails
			
			preloader = new Preloader();
		}

		private function cleanVidItems():void
		{
			//////clears the netClient, video and netstream//////
			if (netClient)
			{
				netClient = {};
				netClient = null;
			}
			if (vid)
			{
				vid = null;
			}
			if (ns)
			{
				ns.removeEventListener(NetStatusEvent.NET_STATUS, netStatusHandler);
				ns.bufferTime = 0;
				ns.close();
				ns = null;
			}
			if (nc)
			{
				nc.close();
				nc = null;
			}
		}

		private function metaHandler(metaData:Object):void
		{
			//////NS will buffer the entire movie. Video sizing = to the source video//////
			ns.bufferTime = metaData.duration;
			vid.width = metaData.width;
			vid.height = metaData.height;

			//////Resize the video if needed and center//////
			if (vid.width >= boxWidth || vid.height >= 740)
			{
				vid = MediaResizer.ResizeItem(vid,740,boxWidth - 50);
			}
			vid.x = 0;//resets the video X position
			vid.x = owner.upperInfo2.width / 2 - vid.width / 2;
			vid.y = owner.upperInfo2.imagePlacer.y + owner.upperInfo2.imagePlacer.height / 2 - vid.height / 2;
		}

		private function netStatusHandler(e:NetStatusEvent):void
		{
			//////sets looping//////
			if (e.info.code == "NetStream.Play.Stop")
			{
				ns.seek(0);
			}
		}

		public function activateThumbs():void
		{
			//////Create video controls//////
			controls = new VidControls();
			this.addChild(controls);
			controls.x = 277;
			controls.y = yButtonLoc;
			controls.playBtn.visible = false;

			//////Sets Index to 0 and displays the first image//////
			currentIndex = 0;
			var ci:int = int(currentIndex);
			loadVideo(ci);
			prev_btn.alpha = 1;
			next_btn.alpha = 1;
			owner.contDisplay.visible = true;
			owner.contDisplay.contNum.text = String(currentIndex+1);
			owner.contDisplay.outOf.text = String(noofVid);

			//////Loads each thumb into the gallery//////
			for (var i:Number = 0; i < noofVid; i++)
			{
				//////Sets the initial thumbnail properties and adds them to array and stage//////
				var thumbC:VidThumb = new VidThumb();
				thumbContainer.x = thumbMask.x - 24;
				thumbContainer.y = thumbMask.y - 5;
				thumbC.vidNum.text = String(i+1);
				thumbArray.push(thumbC);
				thumbContainer.addChild(thumbC);
				addChild(thumbContainer);
			}

			//////add listeners and modes//////
			prev_btn.mouseEnabled = true;
			next_btn.mouseEnabled = true;
			prev_btn.buttonMode = true;
			next_btn.buttonMode = true;
			controls.buttonMode = true;
			controls.addEventListener(MouseEvent.CLICK,controlClick);
			prev_btn.addEventListener(MouseEvent.CLICK,goBackward);
			next_btn.addEventListener(MouseEvent.CLICK,goForward);
			this.addEventListener(Event.ENTER_FRAME, init);
		}

		public function deactivateThumbs():void
		{
			//////Removes all thumbs and clears thumbArray//////
			owner.contDisplay.visible = false;
			var len:int = thumbArray.length;
			for (var i:int = 0; i < len; i++)
			{
				thumbContainer.removeChild(thumbArray[i]);
			}
			thumbArray.length = 0;

			//////Removes all videos, controls and text. Does not remove arrays and stored information//////
			while (videoHolder.numChildren > 0)
			{
				videoHolder.removeChildAt(0);
				controls.buttonMode = false;
				controls.removeEventListener(MouseEvent.CLICK,controlClick);
				this.removeChild(controls);
			}
			cleanVidItems();
			owner.upperInfo2.captionText.text = "";
			owner.upperInfo2.copyrightText.text = "";

			//////remove listeners and modes//////
			prev_btn.visible = false;
			next_btn.visible = false;
			prev_btn.mouseEnabled = false;
			next_btn.mouseEnabled = false;
			prev_btn.buttonMode = false;
			next_btn.buttonMode = false;
			prev_btn.removeEventListener(MouseEvent.CLICK,goBackward);
			next_btn.removeEventListener(MouseEvent.CLICK,goForward);
		}
		private function init(event:Event):void
		{	
			//////Set up preloader math for how much of the video is loaded//////
			if (isLoading == true) {
				var fbl:Number = ns.bytesLoaded;
   				var fbt:Number = ns.bytesTotal; 
				var fper:Number = Math.floor(fbl/fbt*100);
				preloader.percentText.text = fper + "%";
				preloader.percentBar.scaleX = fper/100;
				
				//////If video is fully loaded, remove preloader and play the video//////
				if (fper >= 100 && videoPath != "") {
					isLoading = false;
					preloader.percentText.text = "";
					owner.upperInfo2.removeChild(preloader);
       				ns.resume();
    			}
    		}
			
			//////Hides and shows the buttons depending on thumbnail positions//////
			if (currentIndex <= 0)
			{
				prev_btn.visible = false;
			}
			else
			{
				prev_btn.visible = true;
			}
			if (currentIndex >= noofVid -1)
			{
				next_btn.visible = false;
			}
			else
			{
				next_btn.visible = true;
			}
			if (prev_btn.alpha == 0)
			{
				prev_btn.visible = false;
				next_btn.visible = false;
			}

			//////Prevents looping and sets the thumbnail X and Y proportionally to the adjacent thumbnail//////
			if (thumbArray.length > 0)
			{
				for (var i:Number = 0; i < noofVid; i++)
				{
					if (i==0)
					{
						thumbArray[i].x = 0;
					}
					else
					{
						thumbArray[i].x = thumbArray[i - 1].x + thumbArray[i - 1].width + 25;
					}

					thumbArray[i].y =  -  thumbArray[i].height / 2;

					/////////If no rollOver, highlight the thumb right in the middle////////
					if (currentIndex == i)
					{
						thumbArray[i].filters = glowArray;
					}
					else
					{
						thumbArray[i].filters = [];
					}
				}
			}
		}

		public function loadVideo(i:int):void
		{
			//////Clean out previous video//////
			while (videoHolder.numChildren > 0)
			{
				videoHolder.removeChildAt(0);
			}
			cleanVidItems();
			
			//////Creates the preloader//////
			owner.upperInfo2.addChild(preloader);
			preloader.x = owner.upperInfo2.width / 2;
			preloader.y = owner.upperInfo2.imagePlacer.y + owner.upperInfo2.imagePlacer.height / 2;
			isLoading = true;

			//////Creates new video and adds it to the top box//////			
			videoPath = String(menuVideo[i]);
			nc = new NetConnection();
			nc.connect(null);
			ns = new NetStream(nc);
			vid = new Video();
			videoHolder.addChild(vid);
			vid.attachNetStream(ns);
			netClient = {};
			ns.client = netClient;
			netClient.onMetaData = metaHandler;
			ns.addEventListener(NetStatusEvent.NET_STATUS, netStatusHandler);
			ns.play(videoPath);
			ns.pause();
			
			//////Shows appropriate controls button//////
			controls.playBtn.visible = false;
			controls.pauseBtn.visible = true;
			videoToggle = true;
			
			//////Show the appropriate caption and copyright text//////
			owner.upperInfo2.captionText.text = captionArray[i];
			owner.upperInfo2.copyrightText.text = copyrightArray[i];
			owner.upperInfo2.addChild(videoHolder);
		}

		private function controlClick(event:MouseEvent):void
		{
			//////when clicked if playing, hide play btn and pause//////
			ns.togglePause();
			if (videoToggle == true)
			{
				videoToggle = false;
				controls.playBtn.visible = true;
				controls.pauseBtn.visible = false;
			}
			//////when clicked if paused, hide pause btn and play//////
			else if (videoToggle == false)
			{
				videoToggle = true;
				controls.pauseBtn.visible = true;
				controls.playBtn.visible = false;
			}
		}
		
		private function goForward(event:MouseEvent):void
		{
			//////Slides thumbs to the left. Prevents looping. Then calls to make large video = to the current thumb//////
			if (currentIndex < noofVid)
			{
				currentIndex++;
				owner.contDisplay.contNum.text = String(currentIndex+1);
				var diff:Number = thumbContainer.x - 56 - 20;
				TweenNano.to(thumbContainer,0.2,{x:diff,ease:Strong.easeOut});
			}
			var i:int = int(currentIndex);
			loadVideo(i);
		}

		private function goBackward(event:MouseEvent):void
		{
			//////Slides thumbs to the right. Prevents looping. Then calls to make large video = to the current thumb//////
			if (currentIndex > 0)
			{
				currentIndex--;
				owner.contDisplay.contNum.text = String(currentIndex+1);
				var diff:Number = thumbContainer.x + 56 + 20;
				TweenNano.to(thumbContainer,0.2,{x:diff,ease:Strong.easeOut});
			}
			var i:int = int(currentIndex);
			loadVideo(i);
		}
	}
}