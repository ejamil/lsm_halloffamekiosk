﻿package code
{
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	
	import gs.*;
	import gs.easing.*;

	public class LowerButton extends MovieClip
	{

		public var next_btn:NextButton;
		public var prev_btn:PrevButton;
		public var isOn:Boolean;
		public var isReverse:int;
		public var currentIndex:Number;
		private var noofCont:int;//Number of contents 
		private var infoArray:Array = new Array();
		private var owner:UpperSearchBG;
		private var type:String;

		private var yButtonLoc:Number = 102;

		public function LowerButton(_type:String, needsButtons:Boolean, myOwner:UpperSearchBG)
		{
			//////Reference to the parent class//////
			owner = myOwner;

			if (needsButtons == true)
			{
				prev_btn = new PrevButton();
				this.addChild(prev_btn);
				prev_btn.x = 120;
				prev_btn.y = yButtonLoc;
				prev_btn.alpha = 0;

				next_btn = new NextButton();
				this.addChild(next_btn);
				next_btn.x = this.width - 75;
				next_btn.y = yButtonLoc;
				next_btn.alpha = 0;
			}

			prev_btn.addEventListener(MouseEvent.CLICK,goBackward);
			next_btn.addEventListener(MouseEvent.CLICK,goForward);
			owner.contDisplay.visible = false;

			//////Populate Arrays//////
			type = _type;
		}

		private function init(event:Event):void
		{
			//////Hides and shows the buttons depending on thumbnail positions//////
			if (currentIndex <= 0)
			{
				prev_btn.visible = false;
			}
			else
			{
				prev_btn.visible = true;
			}
			if (currentIndex >= noofCont -1)
			{
				next_btn.visible = false;
			}
			else
			{
				next_btn.visible = true;
			}

			if (prev_btn.alpha == 0)
			{
				prev_btn.visible = false;
				next_btn.visible = false;
			}
		}

		public function activateMenu():void
		{
			//////Reset assets//////
			prev_btn.alpha = 0;
			next_btn.alpha = 0;
			TweenMax.to(prev_btn,.5,{alpha:1,ease:Sine.easeInOut});
			TweenMax.to(next_btn,.5,{alpha:1,ease:Sine.easeInOut});
			currentIndex = 0;
			owner.contDisplay.visible = true;

			if (type == "BIOGRAPHY")
			{
				infoArray = owner.tempBioArray;
			}
			if (type == "NOTABLE STATISTICS")
			{
				infoArray = owner.tempNoteArray;
			}
			if (type == "QUOTES")
			{
				infoArray = owner.tempQuotesArray;
			}
			if (type == "LOUISIANA TIES")
			{
				infoArray = owner.tempTiesArray;
			}
			noofCont = infoArray.length;
			owner.contDisplay.contNum.text = String(currentIndex+1);
			owner.contDisplay.outOf.text = String(noofCont);

			if (noofCont > 1)
			{
				prev_btn.visible = false;
				next_btn.visible = true;
			}

			//////Add listeners and modes//////
			prev_btn.mouseEnabled = true;
			next_btn.mouseEnabled = true;
			prev_btn.buttonMode = true;
			next_btn.buttonMode = true;
			prev_btn.addEventListener(MouseEvent.CLICK,goBackward);
			next_btn.addEventListener(MouseEvent.CLICK,goForward);
			this.addEventListener(Event.ENTER_FRAME, init);
		}
		
		public function activateFinish():void
		{
			//////Once buttons are added start init finction//////
			TweenMax.to(prev_btn,.5,{alpha:1,ease:Sine.easeInOut});
			TweenMax.to(next_btn,.5,{alpha:1,ease:Sine.easeInOut, onComplete:activateFinish});
			this.addEventListener(Event.ENTER_FRAME, init);
		}

		public function deactivateMenu():void
		{
			//////Remove listeners and modes//////
			owner.contDisplay.visible = false;
			prev_btn.alpha = 0;
			next_btn.alpha = 0;
			prev_btn.mouseEnabled = false;
			next_btn.mouseEnabled = false;
			prev_btn.buttonMode = false;
			next_btn.buttonMode = false;
			prev_btn.removeEventListener(MouseEvent.CLICK,goBackward);
			next_btn.removeEventListener(MouseEvent.CLICK,goForward);
		}

		private function goForward(event:MouseEvent):void
		{
			//////changes the text field to the previous page//////
			if (noofCont > 1 && currentIndex < noofCont - 1)
			{
				owner.lowerTextField.text = infoArray[currentIndex + 1];
				currentIndex++;
				owner.contDisplay.contNum.text = String(currentIndex+1);
			}
		}

		private function goBackward(event:MouseEvent):void
		{
			//////changes the text field to the next page//////
			if (noofCont > 1 && currentIndex > 0)
			{
				owner.lowerTextField.text = infoArray[currentIndex - 1];
				currentIndex--;
				owner.contDisplay.contNum.text = String(currentIndex+1);
			}
		}
	}
}