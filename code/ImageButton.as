﻿package code
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	import flash.display.Loader;
	import flash.events.ProgressEvent;
	import flash.net.URLRequest;
	import flash.net.URLLoader;
	import flash.display.LoaderInfo;
	import flash.filters.*;
	import flash.geom.Point;

	import gs.*;
	import gs.easing.*;
	import code.UpperSearchBG;

	public class ImageButton extends MovieClip
	{
		private var menuImages:Array = new Array();
		private var imageSizeArray:Array = new Array();
		private var thumbArray:Array = new Array();
		private var captionArray:Array = new Array();
		private var copyrightArray:Array = new Array();
		public var next_btn:NextButton;
		public var prev_btn:PrevButton;
		private var yButtonLoc:Number = 102;
		private var imageHolder:Sprite = new Sprite;   //add large image in to the Sprite
		private var thumbHolder:Sprite = new Sprite;   //add thum images in to this Sprite
		public var thumbContainer:Sprite = new Sprite; //add thumbHolder images in to the container
		private var noofImg:int; //Number of images 
		private var space:int = 100;
		private var owner:UpperSearchBG;
		private var boxWidth:Number;
		private var boxHeight:Number;
		public var currentIndex:Number;
		private var glowArray:Array = [new GlowFilter()];
		public var isReverse:int;
		private var diff:Number;

		public function ImageButton(_menuImages:Array, _captionsArray:Array, _copyrightArray:Array, needsButtons:Boolean, myOwner:UpperSearchBG):void
		{
			//////Reference to the parent class//////
			owner = myOwner;

			//////Set glow filter properties////// 
			glowArray[0].color = 0xFFFF00;
			glowArray[0].blurX = 15;
			glowArray[0].blurY = 15;

			//////Adds next and previous buttons and sets their properties//////
			if (needsButtons = true)
			{
				prev_btn = new PrevButton();
				this.addChild(prev_btn);
				prev_btn.x = 120;
				prev_btn.y = yButtonLoc;
				prev_btn.addEventListener(MouseEvent.CLICK,goBackward);

				next_btn = new NextButton();
				this.addChild(next_btn);
				next_btn.x = this.width - 75;
				next_btn.y = yButtonLoc;
				next_btn.addEventListener(MouseEvent.CLICK,goForward);
			}

			prev_btn.visible = false;
			next_btn.visible = false;

			//////Set max width and height of the top image//////
			boxWidth = owner.upperInfo2.width;
			boxHeight = owner.upperInfo2.height;

			//////Populate Arrays//////
			menuImages = _menuImages;
			captionArray = _captionsArray;
			copyrightArray = _copyrightArray;

			noofImg = menuImages.length;//get number of images
			thumbContainer.mask = thumbMask;//set mask on thumbnails
		}
		
		public function activateThumbs():void
		{
			//////Sets Index to 0 and displays the first image//////
			currentIndex = 0;
			var ci:int = int(currentIndex);
			showImage(ci);
			isReverse = 3;
			prev_btn.alpha = 1;
			next_btn.alpha = 1;
			owner.contDisplay.visible = true;
			owner.contDisplay.contNum.text = String(currentIndex+1);
			owner.contDisplay.outOf.text = String(noofImg);

			//////Loads each thumb into the gallery//////
			for (var i:Number = 0; i < noofImg; i++)
			{
				var thumbloader:Loader = new Loader ;
				//listner to check when thumb images load complete
				thumbloader.contentLoaderInfo.addEventListener(Event.COMPLETE,loadThumb);
				//load thum images to thumbloader variable;
				thumbloader.load(new URLRequest(menuImages[i]));
				//name property improtant for indentify the each of the thumb images;
				thumbloader.name = i.toString();
				thumbloader.visible = true;
			}
			
			//////remove listeners and modes//////
			prev_btn.mouseEnabled = true;
			next_btn.mouseEnabled = true;
			prev_btn.buttonMode = true;
			next_btn.buttonMode = true;
			prev_btn.addEventListener(MouseEvent.CLICK,goBackward);
			next_btn.addEventListener(MouseEvent.CLICK,goForward);
			this.addEventListener(Event.ENTER_FRAME, init);
		}
		public function deactivateThumbs():void
		{
			//////Removes all thumbs and clears thumbArray//////
			owner.contDisplay.visible = false;
			var len:int = thumbArray.length;
			for (var i:int = 0; i < len; i++)
			{
				thumbContainer.removeChild(thumbArray[i]);
			}
			thumbArray.length = 0;
			
			//////Removes all images and text. Does not remove arrays and stored information//////
			while (imageHolder.numChildren > 0)
			{
				imageHolder.removeChildAt(0);
			}
			owner.upperInfo2.captionText.text = "";
			owner.upperInfo2.copyrightText.text = "";
			
			//////remove listeners and modes//////
			prev_btn.visible = false;
			next_btn.visible = false;
			prev_btn.mouseEnabled = false;
			next_btn.mouseEnabled = false;
			prev_btn.buttonMode = false;
			next_btn.buttonMode = false;
			prev_btn.removeEventListener(MouseEvent.CLICK,goBackward);
			next_btn.removeEventListener(MouseEvent.CLICK,goForward);
		}
		private function init(event:Event):void
		{
			//////sort the thumbArray in ascending order to match the currentIndex//////
			thumbArray.sortOn("name", Array.NUMERIC); 

			//////Hides and shows the buttons depending on thumbnail positions//////
			if (currentIndex <= 0)
			{
				prev_btn.visible = false;
			}
			else
			{
				prev_btn.visible = true;
			}
			if (currentIndex >= noofImg -1)
			{
				next_btn.visible = false;
			}
			else
			{
				next_btn.visible = true;
			}
			if(prev_btn.alpha == 0){
				prev_btn.visible = false;
				next_btn.visible = false;
			}

			//////Prevents looping and sets the thumbnail X and Y proportionally to the adjacent thumbnail//////
			if (thumbArray.length > 0)
			{
				for (var i:Number = 0; i < thumbArray.length; i++)
				{
					if (i==0)
					{
						//////If the first image is thin, adjust it and center//////
						if (thumbArray[i].width < 68)
						{
							diff = (90 - (thumbArray[i].width/2))/2;
							thumbArray[i].x = diff/2;
						}
						//////Otherwise, just center the image//////
						else
						{
							thumbArray[i].x = 0;
						}
					}
					else
					{
						//////If an image is thin, adjust it and center//////
						if (thumbArray[i].width < 68)
						{
							diff = (90 - (thumbArray[i].width/2))/2;
							thumbArray[i].x = diff/2 + thumbArray[i - 1].x + (90);
						}
						//////If the image before it was thin, adjust it and center//////
						else if (thumbArray[i-1].width < 68)
						{
							thumbArray[i].x = thumbArray[i - 1].x - diff/2 + 90;
						}
						//////Otherwise, just center the image//////
						else
						{
							thumbArray[i].x = thumbArray[i - 1].x + 90;
						}
					}
					
					//////Center the images vertically in the image bar//////
					thumbArray[i].y =  -  thumbArray[i].height / 2;

					/////////If no rollOver, highlight the thumb right in the middle////////
					if (currentIndex == i)
					{
						thumbArray[i].filters = glowArray;
					}
					else
					{
						thumbArray[i].filters = [];
					}
				}
			}
		}
		
		private function showImage(i:int):void
		{
			//////creates listener and adds large image picture, caption text and copyright text//////
			var loader:Loader = new Loader  ;
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE,loadImage);
			loader.load(new URLRequest(menuImages[i]));
			owner.upperInfo2.captionText.text = captionArray[i];
			owner.upperInfo2.copyrightText.text = copyrightArray[i];
		}

		private function loadThumb(event:Event):void
		{
			//////Sets the initial thumbnail properties and adds them to array and stage//////
			var thumbC:MovieClip = new MovieClip();
			thumbContainer.x = thumbMask.x - 36;
			thumbContainer.y = thumbMask.y;
			thumbC.addChild(event.target.loader);
			thumbC.name = event.target.loader.name;
			thumbArray.push(thumbC);
			thumbContainer.addChild(thumbC);
			addChild(thumbContainer);

			//////Resize thumbnail images//////
			if (thumbC.width >= 70 || thumbC.height >= 70)
			{
				thumbC = MediaResizer.ResizeItem(thumbC,68,68);
			}
		}

		private function loadImage(event:Event):void
		{
			//////Clean out previous image//////
			while (imageHolder.numChildren > 0)
			{
				imageHolder.removeChildAt(0);
			}
			event.target.loader.alpha = 0;

			//////Adds new large image to the top box//////
			imageHolder.addChild(event.target.loader);
			owner.upperInfo2.addChild(imageHolder);
			TweenNano.to(event.target.loader,2,{alpha:100,ease:Sine.easeInOut});

			//////Resize the new image if needed and center//////
			if (imageHolder.width >= boxWidth || imageHolder.height >= boxHeight)
			{
				imageHolder = MediaResizer.ResizeItem(imageHolder,boxHeight - 465,boxWidth - 50);
			}
			imageHolder.x = 0;//resets the large image X position
			imageHolder.x = owner.upperInfo2.width / 2 - imageHolder.width / 2;
			imageHolder.y = owner.upperInfo2.imagePlacer.y + owner.upperInfo2.imagePlacer.height/2 - imageHolder.height / 2;
		}

		private function goForward(event:MouseEvent):void
		{
			//////Slides thumbs to the left. Prevents looping. Then calls to make large image = to the current thumb//////
			if (currentIndex < noofImg)
			{
				currentIndex++;
				owner.contDisplay.contNum.text = String(currentIndex+1);
				var diff:Number = thumbContainer.x - 90;
				TweenNano.to(thumbContainer,0.2,{x:diff,ease:Strong.easeOut});
			}
			var i:int = int(currentIndex);
			showImage(i);
		}

		private function goBackward(event:MouseEvent):void
		{
			//////Slides thumbs to the right. Prevents looping. Then calls to make large image = to the current thumb//////
			if (currentIndex > 0)
			{
				currentIndex--;
				owner.contDisplay.contNum.text = String(currentIndex+1);
				var diff:Number = thumbContainer.x + 90;
				//TweenMax.to(box,1,{delay:2, alpha:0, x:400, y:300});
			}
			var i:int = int(currentIndex);
			showImage(i);
		}
	}
}