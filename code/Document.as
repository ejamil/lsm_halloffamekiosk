﻿package code
{
	import flash.display.MovieClip;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.events.*;
	import flash.display.*;
	import flash.xml.*;
	import flash.media.Video;
	import flash.net.NetConnection;
	import flash.net.NetStream;
	import flash.ui.Mouse;
	import flash.utils.Timer;
	import flash.events.TimerEvent;
	import code.XmlLoader;

	import gs.*;
	import gs.easing.*;

	public class Document extends MovieClip
	{
		private var bg:BG = new BG();

		private var myStage:MovieClip = new MovieClip();
		public var unsortedXML:XML;
		private var initLoader:URLLoader;
		private var initXML:XML;
		private var xmlLoader:URLLoader;

		private var xOffset:Number = -504;
		private var xLowerOffset:Number = -143;
		private var searchOffsetx:Number = 55;
		private var searchOffsety:Number = 2005;

		public var athlete:Athlete;

		public var athletes:Array = new Array();
		public var categories:Array = new Array();
		public var homeTowns:Array = new Array();
		public var almaMaters:Array = new Array();

		public var athletesNames:Array = new Array();

		public var currentAthletes:Array;

		private var attractLoop:Video;
		private var attractNC:NetConnection = new NetConnection();
		private var attractNS:NetStream;
		private var attractListener:Object = new Object();

		private var lowerLoop:LowerBG;
		private var lowerNC:NetConnection = new NetConnection();
		private var lowerNS:NetStream;
		private var lowerListener:Object = new Object();

		private var upperLoop:Video;
		private var upperNC:NetConnection = new NetConnection();
		private var upperNS:NetStream;
		private var upperListener:Object = new Object();

		private var topWindow:twoScreens;
		private var bottomWindow:twoScreens;
		private var upperSearchBG:UpperSearchBG;

		private var alphabetSliderBar:AlphabetSliderBar;
		private var searchName:SearchName;
		private var searchHometown:SearchHometown;
		private var searchAlmaMater:SearchAlmaMater;
		private var searchSport:SearchSport;
		private var back_btn:BackButton;
		private var mainInfoBox:InfoBox;
		private var secondInfoBox:InfoBox;
		private var mainInfoSlider:InfoSlider;
		private var secondInfoSlider:InfoSlider;
		private var helperText:HelperText;

		private var nLine:NameLine;
		private var sLine:SportLine;
		private var hLine:NameLine;
		private var aLine:SportLine;

		private var currentLetter:String;
		public var chosenCategory:String;

		private var searchArray:Array = new Array();
		private var timeoutTimer:Timer;
		private var whichButton:String;
		private var infoBoxArray:Array = new Array();

		public var finalNameSet:Boolean = false;
		public var isClicked:Boolean = false;
		public var attractOn:Boolean = false;
		public var timerBoolean:Boolean = false;
		private var nameClicked:Boolean = false;
		private var homeClicked:Boolean = false;
		private var almaClicked:Boolean = false;
		private var sportClicked:Boolean = false;

		public function Document()
		{
			//////create the main page and load the xml//////
			topWindow = new twoScreens(0,0,1366,2706);
			topWindow.addChildToWindow(myStage);

			addAttractLoop();
			addLowerLoop();
			addMainButtons();
			
			loaderInfo.addEventListener(Event.COMPLETE, loadConfig);
					
			trace("Hello world!");
			//Mouse.hide();
		}
		
		private function loadConfig(e:Event):void
		{			
			//////Load the initial XML and call the main XML//////
			initLoader = new URLLoader();
			initLoader.load(new URLRequest("cms_config.xml"));
			initLoader.addEventListener(Event.COMPLETE, processInitXML);
		}
		
		public function processInitXML(e:Event):void
		{
			//////Load the main XML and remove listener//////
			initXML = new XML(e.target.data);
			initLoader.removeEventListener(Event.COMPLETE, processInitXML);

			xmlLoader = new URLLoader();
			xmlLoader.load(new URLRequest(initXML.cms_folder));
			xmlLoader.addEventListener(Event.COMPLETE, processXML);
		}
		
		private function processXML(e:Event):void
		{
			//////build each athlete and play in athletes array//////
			unsortedXML = new XML(e.target.data);
			var hofXML:XML = new XML(sortXMLByAttribute(unsortedXML,"Last_Name"));
			for (var i = 0; i < hofXML.nodes.node.length(); i++)
			{
				athlete = new Athlete(hofXML.nodes.node[i].First_Name,
				  hofXML.nodes.node[i].Maiden_Name,
				  hofXML.nodes.node[i].Last_Name,
				  hofXML.nodes.node[i].Categories, 
				  hofXML.nodes.node[i].Sport_Name_League,
				  hofXML.nodes.node[i].Year_Inducted,
				  hofXML.nodes.node[i].Nicknames,
				  hofXML.nodes.node[i].Home_Town,
				  hofXML.nodes.node[i].lifespan.birth,
				  hofXML.nodes.node[i].lifespan.death,
				  hofXML.nodes.node[i].Alma_Maters,
				  hofXML.nodes.node[i].Louisiana_Ties,
				  hofXML.nodes.node[i].Biography_and_Achievements,
				  hofXML.nodes.node[i].Notable_Statistics,
				  hofXML.nodes.node[i].Other_Halls_of_Fames,
				  hofXML.nodes.node[i].Quotes,
				  hofXML.nodes.node[i].Official_Portrait,
				  hofXML.nodes.node[i].Other_Photos,
				  hofXML.nodes.node[i].Photos_of_Artifacts_Provided_for_Inductee,
				  hofXML.nodes.node[i].Videos,
				  hofXML.nodes.node[i].Audios);
				athlete.outputAthlete();
				athletes.push(athlete);
				athletesNames.push(athlete.fullName);
				trace(athlete.fullName);
			}

			//////build arrays//////
			buildHomeTownArray();
			buildAlmaMaterArray();
			buildCategoriesArray();
		}

		private function addLowerLoop():void
		{
			//////create the lower loop//////
			lowerLoop = new LowerBG();
			myStage.addChild(lowerLoop);
			lowerLoop.y = 1920;
			lowerLoop.x = xOffset + xLowerOffset;
		}

		private function addAttractLoop():void
		{
			//////create the attract loop//////			
			attractLoop = new Video(1080,1920);
			myStage.addChild(attractLoop);
			attractLoop.x = xOffset;

			attractNC.connect(null);

			attractNS = new NetStream(attractNC);
			attractLoop.attachNetStream(attractNS);
			
			attractNS.client = {onMetaData:ns_onMetaData,NetStatusEvent:ns_onPlayStatus};
			attractNS.play("lsmAttractloop.f4v");
			this.addEventListener(Event.ENTER_FRAME, init);
			attractOn = true;
		}

		private function ns_onMetaData(e:Object):void
		{
			// Resize video instance.
			//attractLoop.width = e.width;
			//attractLoop.height = e.height;
			// Center video instance on Stage.
			//attractLoop.x = (stage.stageWidth - attractLoop.width) / 2;
			//attractLoop.y = (stage.stageHeight - attractLoop.height) / 2;
		}

		private function ns_onPlayStatus(e:NetStatusEvent):void
		{
			//////loop the attract video//////
			if (e.info.code == "NetStream.Play.Stop")
			{
				attractNS.seek(0);
			}
		}

		private function ns_loweronPlayStatus(e:NetStatusEvent):void
		{
			//////loop the lower video//////
			if (e.info.code == "NetStream.Play.Stop")
			{
				lowerNS.seek(0);

				lowerLoop.attachNetStream(null);
				lowerLoop.attachNetStream(lowerNS);
			}
		}

		private function ns_upperonPlayStatus(e:NetStatusEvent):void
		{
			//////loop the upper video//////
			if (e.info.code == "NetStream.Play.Stop")
			{
				upperNS.seek(0);
			}
		}
		
		private function init(event:Event):void
		{	
			//////If attract loop is supposed to be playing, when it gets to the end, loop it//////
			if(attractOn == true)
			{
				if(attractNS.time >= 18.1){
					attractNS.close();
					attractNS.play("lsmAttractloop.f4v");
				}
			}
			//////If background video is supposed to be playing, when it gets to the end, loop it//////
			if(attractOn == false)
			{
				if(attractNS.time >= 18.1){
					attractNS.close();
					attractNS.play("lsmBG.f4v");
				}
			}
		}

		public static function sortXMLByAttribute(xml:XML, attribute:String, options:Object = null, copy:Boolean = false):XML
		{
			//////create the xml arrays and populate it//////
			var xmlArray : Array = new Array();

			for (var i:int = 0; i < xml.node.length(); i++)
			{
				xmlArray.push(xml.node[i]);
			}
			xmlArray.sortOn('Last_Name', options);
			var sortedXmlList:XMLList = new XMLList("<nodes />");

			//////append the xml back into a new xml document and sort it//////
			for (var j:int = 0; j < xmlArray.length; j++)
			{
				sortedXmlList.appendChild(xmlArray[j]);
			}
			if (copy)
			{
				return xml.copy().setChildren(sortedXmlList);
			}
			else
			{
				return xml.setChildren(sortedXmlList);
			}
		}

		private function addMainButtons():void
		{
			//////add each of the 4 main buttons and set their properties//////
			searchName = new SearchName();
			searchName.bg.bgTitle.btnTitle.text = "Search By Name";
			nLine = new NameLine();
			myStage.addChild(nLine);
			myStage.addChild(searchName);
			searchArray.push(searchName);

			searchSport = new SearchSport();
			searchSport.bg.bgTitle.btnTitle.text = "Search By Sport";
			sLine = new SportLine();
			myStage.addChild(sLine);
			myStage.addChild(searchSport);
			searchArray.push(searchSport);

			searchAlmaMater = new SearchAlmaMater();
			searchAlmaMater.bg.bgTitle.btnTitle.text = "Search By Alma Mater";
			aLine = new SportLine();
			myStage.addChild(aLine);
			myStage.addChild(searchAlmaMater);
			searchArray.push(searchAlmaMater);

			searchHometown = new SearchHometown();
			searchHometown.bg.bgTitle.btnTitle.text = "Search By Hometown";
			hLine = new NameLine();
			myStage.addChild(hLine);
			myStage.addChild(searchHometown);
			searchArray.push(searchHometown);
			
			helperText = new HelperText();
			helperText.x = 406;
			helperText.y = 2300;
			myStage.addChild(helperText);
			
			//////Add the intro button tweens. First one delayed and the rest staggered//////
			var tweenClose:TimelineMax = new TimelineMax();
			tweenClose.append(TweenMax.to(searchName.bg, 1, {frame:25, ease:Linear.easeNone}), .5);
			tweenClose.append(TweenMax.to(searchSport.bg, 1, {frame:25, ease:Linear.easeNone}), -.75);
			tweenClose.append(TweenMax.to(searchAlmaMater.bg, 1, {frame:25, ease:Linear.easeNone}), -.75);
			tweenClose.append(TweenMax.to(searchHometown.bg, 1, {frame:25, ease:Linear.easeNone, onComplete:introFinish}), -.75);

			var startSearchY = searchOffsety;
			for (var i = 0; i < searchArray.length; i++)
			{
				searchArray[i].x = xOffset - 100;
				searchArray[i].y = startSearchY - 25 + searchArray[i].height + 110;
				startSearchY = searchArray[i].y;
			}
			
			//////set position of the line tweens//////
			nLine.x = searchName.x + searchName.width - 25;
			nLine.y = searchName.y + searchName.height / 2;
			sLine.x = searchSport.x + searchSport.width - 25;
			sLine.y = searchSport.y + searchSport.height / 2;
			hLine.x = searchHometown.x + searchHometown.width - 25;
			hLine.y = searchHometown.y + searchHometown.height / 2;
			aLine.x = searchAlmaMater.x + searchAlmaMater.width - 25;
			aLine.y = searchAlmaMater.y + searchAlmaMater.height / 2;
			aLine.scaleY *= -1;
			hLine.scaleY *= -1;
		}
		
		public function introFinish():void
		{
			//////when the intro tween is done turn on the event listeners//////
			searchName.addEventListener(MouseEvent.CLICK, nameSearch);
			searchSport.addEventListener(MouseEvent.CLICK, sportSearch);
			searchAlmaMater.addEventListener(MouseEvent.CLICK, almaMaterSearch);
			searchHometown.addEventListener(MouseEvent.CLICK, homeTownSearch);
			
			//////Set the timeoutTimer  for 2 minutes and start it//////
			timeoutTimer = new Timer(120000,1); //5000 for testing
			timeoutTimer.addEventListener(TimerEvent.TIMER, onTimeout);
			myStage.addEventListener(MouseEvent.CLICK, resetTimeout);
		}

		public function setLetter(myCurrentLetter:String):void
		{
			//////change the letter shown//////
			if (currentLetter != myCurrentLetter)
			{
				currentLetter = myCurrentLetter;
				var currentAthletes:Array = new Array();
				for (var i:int = 0; i < athletes.length; i++)
				{
					if (athletes[i].lastName.charAt(0) == currentLetter)
					{
						currentAthletes.push(athletes[i].aDisplayName);
					}
				}
				checkSecondBox();
				addInfoBox(searchName.x + searchName.width - 50, searchName.y + 15, currentAthletes);
			}
		}

		public function setNames(currentLetter:String):void
		{
			//////populate the infobox with the athlete names of the chosen letter//////
			var currentAthletes:Array = new Array();
			for (var i:int = 0; i < athletes.length; i++)
			{
				if (athletes[i].lastName.charAt(0) == currentLetter)
				{
					currentAthletes.push(athletes[i].aDisplayName);
				}
			}
			checkSecondBox();
			addInfoBox(searchName.x + searchName.width - 50, searchName.y + 15, currentAthletes);
		}
		
		private function nameSearch(e:MouseEvent):void
		{
			//////start the linetween and close all other tweens//////
			finalNameSet = false;
			isClicked = false;
			helperText.gotoAndStop(2);
			nLine.visible = true;
			
			//////remove the secondary box//////
			var tweenClose:TimelineMax = new TimelineMax();
	
			if (secondInfoBox != null)
			{
				nLine.lineBlur.visible = true;
				tweenClose.append(TweenMax.to(secondInfoBox,.1,{scaleX:0,ease:Sine.easeInOut}));
				tweenClose.append(TweenMax.to(mainInfoBox.infoLine,.3,{x:235,ease:Sine.easeInOut, onComplete:checkSecondBox}));
				tweenClose.insert(TweenMax.to(searchName.bg.bgWhite,.3,{alpha:1,ease:Sine.easeInOut}));
				tweenClose.append(TweenMax.to(nLine.nMask,.3,{x:23,ease:Linear.easeNone, onComplete:nameFinish}));
				tweenClose.insert(TweenMax.to(nLine.lineBlur, .25, {frame:12, startAt:{frame:1}, ease:Linear.easeNone}), .75);
			} 
			else
			{
				nLine.lineBlur.visible = true;
				tweenClose.insert(TweenMax.to(searchName.bg.bgWhite,.3,{alpha:1,ease:Sine.easeInOut}));
				tweenClose.append(TweenMax.to(nLine.nMask,.3,{x:23,ease:Linear.easeNone, onComplete:nameFinish}));
				tweenClose.insert(TweenMax.to(nLine.lineBlur, .25, {frame:12, startAt:{frame:1}, ease:Linear.easeNone}), .3);
			}

			TweenNano.to(sLine.nMask,.3,{x:-210,ease:Sine.easeInOut});
			TweenNano.to(aLine.nMask,.3,{x:-210,ease:Sine.easeInOut});
			TweenNano.to(hLine.nMask,.3,{x:-210,ease:Sine.easeInOut});
			TweenNano.to(searchSport.bg.bgWhite,.3,{alpha:0,ease:Sine.easeInOut});
			TweenNano.to(searchHometown.bg.bgWhite,.3,{alpha:0,ease:Sine.easeInOut});
			TweenNano.to(searchAlmaMater.bg.bgWhite,.3,{alpha:0,ease:Sine.easeInOut});
		}
		
		private function nameFinish():void
		{
			//////toggle which button and add info box and a slider//////
			nLine.lineBlur.visible = false;
			whichButton = "Names";
			addAlphabetSlider();
			var currentAthletes:Array = new Array();
			for (var i:int = 0; i < athletes.length; i++)
			{
				if (athletes[i].lastName.charAt(0) == currentLetter)
				{
					currentAthletes.push(athletes[i].aDisplayName);
				}
			}			
			addInfoBox(searchName.x + searchName.width - 50, searchName.y, currentAthletes);
		}

		private function sportSearch(e:MouseEvent):void
		{
			//////start the linetween and close all other tweens//////
			var tweenClose:TimelineMax = new TimelineMax();
			helperText.gotoAndStop(3);
			sLine.visible = true;
			
			if (secondInfoBox != null)
			{
				sLine.lineBlur.visible = true;
				tweenClose.append(TweenMax.to(secondInfoBox,.1,{scaleX:0,ease:Sine.easeInOut}));
				tweenClose.append(TweenMax.to(mainInfoBox.infoLine,.3,{x:235,ease:Sine.easeInOut, onComplete:checkSecondBox}));
				tweenClose.insert(TweenMax.to(searchSport.bg.bgWhite,.3,{alpha:1,ease:Sine.easeInOut}));
				tweenClose.append(TweenMax.to(sLine.nMask,.3,{x:23,ease:Linear.easeNone, onComplete:sportFinish}));
				tweenClose.insert(TweenMax.to(sLine.lineBlur, .25, {frame:12, startAt:{frame:1}, ease:Linear.easeNone}), .75);
			} 
			else
			{
				sLine.lineBlur.visible = true;
				tweenClose.insert(TweenMax.to(searchSport.bg.bgWhite,.3,{alpha:1,ease:Sine.easeInOut}));
				tweenClose.append(TweenMax.to(sLine.nMask,.3,{x:23,ease:Linear.easeNone, onComplete:sportFinish}));
				tweenClose.insert(TweenMax.to(sLine.lineBlur, .25, {frame:12, startAt:{frame:1}, ease:Linear.easeNone}), .3);
			}
			
			TweenNano.to(nLine.nMask,.3,{x:-210,ease:Sine.easeInOut});
			TweenNano.to(aLine.nMask,.3,{x:-210,ease:Sine.easeInOut});
			TweenNano.to(hLine.nMask,.3,{x:-210,ease:Sine.easeInOut});
			TweenNano.to(searchName.bg.bgWhite,.3,{alpha:0,ease:Sine.easeInOut});
			TweenNano.to(searchHometown.bg.bgWhite,.3,{alpha:0,ease:Sine.easeInOut});
			TweenNano.to(searchAlmaMater.bg.bgWhite,.3,{alpha:0,ease:Sine.easeInOut});
		}
		
		private function sportFinish():void
		{
			//////toggle which button and add info box and a slider//////
			sLine.lineBlur.visible = false;
			whichButton = "Sports";
			if (alphabetSliderBar != null)
			{
				alphabetSliderBar.visible = false;
			}
			addInfoBox(searchName.x + searchName.width - 50, searchName.y + 15, categories);
		}

		private function almaMaterSearch(e:MouseEvent):void
		{
			//////start the linetween and close all other tweens//////
			var tweenClose:TimelineMax = new TimelineMax();
			helperText.gotoAndStop(4);
			aLine.visible = true;
			
			if (secondInfoBox != null)
			{
				aLine.lineBlur.visible = true;
				tweenClose.append(TweenMax.to(secondInfoBox,.1,{scaleX:0,ease:Sine.easeInOut}));
				tweenClose.append(TweenMax.to(mainInfoBox.infoLine,.3,{x:235,ease:Sine.easeInOut, onComplete:checkSecondBox}));
				tweenClose.insert(TweenMax.to(searchAlmaMater.bg.bgWhite,.3,{alpha:1,ease:Sine.easeInOut}));
				tweenClose.append(TweenMax.to(aLine.nMask,.3,{x:23,ease:Linear.easeNone, onComplete:almaFinish}));
				tweenClose.insert(TweenMax.to(aLine.lineBlur, .25, {frame:12, startAt:{frame:1}, ease:Linear.easeNone}), .75);
			} 
			else
			{
				aLine.lineBlur.visible = true;
				tweenClose.insert(TweenMax.to(searchAlmaMater.bg.bgWhite,.3,{alpha:1,ease:Sine.easeInOut}));
				tweenClose.append(TweenMax.to(aLine.nMask,.3,{x:23,ease:Linear.easeNone, onComplete:almaFinish}));
				tweenClose.insert(TweenMax.to(aLine.lineBlur, .25, {frame:12, startAt:{frame:1}, ease:Linear.easeNone}), .3);
			}
			
			TweenNano.to(nLine.nMask,.3,{x:-210,ease:Sine.easeInOut});
			TweenNano.to(sLine.nMask,.3,{x:-210,ease:Sine.easeInOut});
			TweenNano.to(hLine.nMask,.3,{x:-210,ease:Sine.easeInOut});
			TweenNano.to(searchName.bg.bgWhite,.3,{alpha:0,ease:Sine.easeInOut});
			TweenNano.to(searchSport.bg.bgWhite,.3,{alpha:0,ease:Sine.easeInOut});
			TweenNano.to(searchHometown.bg.bgWhite,.3,{alpha:0,ease:Sine.easeInOut});
		}
		
		private function almaFinish():void
		{
			//////toggle which button and add info box and a slider//////
			aLine.lineBlur.visible = false;
			whichButton = "Alma Maters";
			if (alphabetSliderBar != null)
			{
				alphabetSliderBar.visible = false;
			}
			addInfoBox(searchName.x + searchName.width - 50, searchName.y + 15, almaMaters);
		}

		private function homeTownSearch(e:MouseEvent):void
		{
			//////start the linetween and close all other tweens//////
			var tweenClose:TimelineMax = new TimelineMax();
			helperText.gotoAndStop(5);
			hLine.visible = true;
			
			if (secondInfoBox != null)
			{
				hLine.lineBlur.visible = true;
				tweenClose.append(TweenMax.to(secondInfoBox,.1,{scaleX:0,ease:Sine.easeInOut}));
				tweenClose.append(TweenMax.to(mainInfoBox.infoLine,.3,{x:235,ease:Sine.easeInOut, onComplete:checkSecondBox}));
				tweenClose.insert(TweenMax.to(searchHometown.bg.bgWhite,.3,{alpha:1,ease:Sine.easeInOut}));
				tweenClose.append(TweenMax.to(hLine.nMask,.3,{x:23,ease:Linear.easeNone, onComplete:homeFinish}));
				tweenClose.insert(TweenMax.to(hLine.lineBlur, .25, {frame:12, startAt:{frame:1}, ease:Linear.easeNone}), .75);
			} 
			else
			{
				hLine.lineBlur.visible = true;
				tweenClose.insert(TweenMax.to(searchHometown.bg.bgWhite,.3,{alpha:1,ease:Sine.easeInOut}));
				tweenClose.append(TweenMax.to(hLine.nMask,.3,{x:23,ease:Linear.easeNone, onComplete:homeFinish}));
				tweenClose.insert(TweenMax.to(hLine.lineBlur, .25, {frame:12, startAt:{frame:1}, ease:Linear.easeNone}), .3);
			}
			
			TweenNano.to(nLine.nMask,.3,{x:-210,ease:Sine.easeInOut});
			TweenNano.to(sLine.nMask,.3,{x:-210,ease:Sine.easeInOut});
			TweenNano.to(aLine.nMask,.3,{x:-210,ease:Sine.easeInOut});
			TweenNano.to(searchName.bg.bgWhite,.3,{alpha:0,ease:Sine.easeInOut});
			TweenNano.to(searchSport.bg.bgWhite,.3,{alpha:0,ease:Sine.easeInOut});
			TweenNano.to(searchAlmaMater.bg.bgWhite,.3,{alpha:0,ease:Sine.easeInOut});
		}
		
		private function homeFinish():void
		{
			//////toggle which button and add info box and a slider//////
			hLine.lineBlur.visible = false;
			whichButton = "Home Towns";
			if (alphabetSliderBar != null)
			{
				alphabetSliderBar.visible = false;
			}
			addInfoBox(searchName.x + searchName.width - 50, searchName.y + 15, homeTowns);
		}

		private function addAlphabetSlider():void
		{
			//////if no top slider add one, otherwise unhide it//////
			if (alphabetSliderBar == null)
			{
				alphabetSliderBar = new AlphabetSliderBar(myStage,this);
				myStage.addChild(alphabetSliderBar);
				alphabetSliderBar.x = xOffset + xLowerOffset;
				alphabetSliderBar.y = 1990;
			}
			alphabetSliderBar.visible = true;
		}

		private function checkSecondBox():void
		{
			//////remove the secondary box//////
			if (secondInfoBox != null)
			{
				//secondInfoBox.visible = false;
				myStage.removeChild(secondInfoBox);
				secondInfoBox = null;
			}
		}

		private function checkMainBox():void
		{
			//////remove the main info box//////
			if (mainInfoBox != null)
			{
				//mainInfoBox.visible = false;
				myStage.removeChild(mainInfoBox);
				mainInfoBox = null;
			}
		}

		public function addInfoBox(infox:Number, infoy:Number, infoArray:Array):void
		{
			//////if no main info box create one ,otherwise load new info//////
			if (mainInfoBox == null)
			{
				mainInfoBox = new InfoBox(infoArray,myStage,this);
				mainInfoBox.scaleX = 0;
				myStage.addChild(mainInfoBox);
				mainInfoBox.x = infox;
				mainInfoBox.y = infoy;
				infoBoxArray.push(mainInfoBox);
				mainInfoBox.infoLine.visible = false;
				TweenMax.to(mainInfoBox,.3,{scaleX:1,ease:Sine.easeInOut});
				//mainInfoBox.scaleTextToFitInTextField(mainInfoBox._itemTextField, 240);
			}
			else
			{
				mainInfoBox.reloadList(infoArray);
				//mainInfoBox.scaleTextToFitInTextField(mainInfoBox._itemTextField, 240);
			}
		}

		private function addSecondInfoBox(infox:Number, infoy:Number, infoArray:Array):void
		{
			//////If no second box create one, otherwise load new info//////
			if (secondInfoBox == null)
			{
				secondInfoBox = new InfoBox(infoArray,myStage,this,true);
				secondInfoBox.x = infox + secondInfoBox.width + 10;
				secondInfoBox.y = infoy;
				secondInfoBox.scaleX = 0;
				myStage.addChild(secondInfoBox);
				infoBoxArray.push(secondInfoBox);
				mainInfoBox.infoLine.visible = true;
				secondInfoBox.infoLine.visible = false;
				
				mainInfoBox.infoLine.lineBlur.alpha = 1;
				TweenMax.to(mainInfoBox.infoLine, .3,{x:320,ease:Sine.easeInOut});
				TweenMax.delayedCall(.3, secondFinish);
				secondInfoBox.reloadList(infoArray);
				//secondInfoBox.scaleTextToFitInTextField(secondInfoBox._itemTextField, 240);
			}
			else
			{
				secondInfoBox.reloadList(infoArray);
				//secondInfoBox.scaleTextToFitInTextField(secondInfoBox._itemTextField, 240);
			}
		}
		
		public function secondFinish():void
		{
			TweenMax.to(secondInfoBox, .3,{scaleX:1,ease:Sine.easeInOut});
			TweenMax.to(mainInfoBox.infoLine.lineBlur, .3,{alpha:0,ease:Sine.easeInOut});
		}

		public function setCurrentValue(currentValue:String):void
		{			
			//////create a secondary box if one is needed//////
			var infoArray:Array = new Array();
			var tempName:String;
			if (finalNameSet == true && isClicked == true)
			{
				setSecondScreen(currentValue);
				isClicked = false;
			}
			
			//////Populate the infoBoxes with information from each category//////
			else if (whichButton == "Home Towns" || whichButton == "Alma Maters" || whichButton == "Sports")
			{
				if (whichButton == "Home Towns")
				{
					for (var i:int = 0; i < athletes.length; i++)
					{
						if (currentValue == athletes[i].homeTown)
						{
							tempName = buildDisplayName(athletes[i]);
							infoArray.push(tempName);//athletes[i].fullName
						}
					}
				}
				else if (whichButton == "Alma Maters")
				{
					for (var j:int = 0; j < athletes.length; j++)
					{
						for (var m:int = 0; m < athletes[j].almaMaterArray.length; m++)
						{
							if (currentValue == athletes[j].almaMaterArray[m])
							{
								tempName = buildDisplayName(athletes[j]);
								infoArray.push(tempName);
							}
						}
					}
				}
				else if (whichButton == "Sports")
				{
					for (var k:int = 0; k < athletes.length; k++)
					{
						for (var l:int = 0; l < athletes[k].categoriesArray.length; l++)
						{
							if (currentValue == athletes[k].categoriesArray[l])
							{
								tempName = buildDisplayName(athletes[k]);
								infoArray.push(tempName);
							}
						}
					}
				}
				chosenCategory = currentValue;
				addSecondInfoBox(mainInfoBox.x, mainInfoBox.y, infoArray);
				isClicked = true;
			}
			if (whichButton == "Names")
			{
				setSecondScreen(currentValue);
				isClicked = false;
			}
		}

		private function setSecondScreen(currentValue:String):void
		{
			//////remove the main button listeners//////
			searchName.removeEventListener(MouseEvent.CLICK, nameSearch);
			searchSport.removeEventListener(MouseEvent.CLICK, sportSearch);
			searchAlmaMater.removeEventListener(MouseEvent.CLICK, almaMaterSearch);
			searchHometown.removeEventListener(MouseEvent.CLICK, homeTownSearch);
			
			//////reset the main video loop//////
			var tempValue:String = currentValue;
			helperText.visible = false;
			//attractLoop.attachNetStream(null);
			attractNS.play("lsmBG.f4v");
			attractOn = false;
			/*upperLoop = new Video(1080,1920);
			myStage.addChild(upperLoop);
			upperLoop.x = xOffset;

			upperNC.connect(null);

			upperNS = new NetStream(upperNC);
			upperLoop.attachNetStream(upperNS);

			upperNS.client = {onMetaData:ns_onMetaData,NetStatusEvent:ns_upperonPlayStatus};
			upperNS.play("lsmBG.f4v");
			upperNS.addEventListener(NetStatusEvent.NET_STATUS, ns_upperonPlayStatus);*/
			
			//////finish the tweens and then build the next screen//////
			/*if (nLine.nMask.x > 0)
			{											
				//////tween.insert or append(tweenType (objectToTween, tweenTime {parameterTweened, easing, oncompleteFunction, onCompleteFunctionParameters}) tweenDelay//////
				var nTweenClose:TimelineMax = new TimelineMax();
				nTweenClose.append(TweenMax.to(mainInfoBox,.5,{scaleX:0,ease:Sine.easeInOut}));
				nTweenClose.append(TweenMax.to(nLine.nMask,.5,{x:-210,ease:Sine.easeInOut}), -.1);
				nTweenClose.append(TweenMax.to(searchName.bg.bgWhite,.5,{alpha:0,ease:Sine.easeInOut, onComplete:checkFinish, onCompleteParams:[tempValue]}));
			}
			if (sLine.nMask.x > 0)
			{
				var sTweenClose:TimelineMax = new TimelineMax();
				sTweenClose.append(TweenMax.to(secondInfoBox,.5,{scaleX:0,ease:Sine.easeInOut}));
				sTweenClose.append(TweenMax.to(mainInfoBox.infoLine,.5,{x:235,ease:Sine.easeInOut}), -.1);
				sTweenClose.append(TweenMax.to(mainInfoBox,.5,{scaleX:0,ease:Sine.easeInOut}), -.1);
				sTweenClose.append(TweenMax.to(sLine.nMask,.5,{x:-210,ease:Sine.easeInOut}), -.1);
				sTweenClose.append(TweenMax.to(searchSport.bg.bgWhite,.5,{alpha:0,ease:Sine.easeInOut, onComplete:checkFinish, onCompleteParams:[tempValue]}));
			}
			if (aLine.nMask.x > 0)
			{
				var aTweenClose:TimelineMax = new TimelineMax();
				aTweenClose.append(TweenMax.to(secondInfoBox,.5,{scaleX:0,ease:Sine.easeInOut}));
				aTweenClose.append(TweenMax.to(mainInfoBox.infoLine,.5,{x:235,ease:Sine.easeInOut}), -.1);
				aTweenClose.append(TweenMax.to(mainInfoBox,.5,{scaleX:0,ease:Sine.easeInOut}), -.1);
				aTweenClose.append(TweenMax.to(aLine.nMask,.5,{x:-210,ease:Sine.easeInOut}), -.1);
				aTweenClose.append(TweenMax.to(searchAlmaMater.bg.bgWhite,.5,{alpha:0,ease:Sine.easeInOut, onComplete:checkFinish, onCompleteParams:[tempValue]}));
			}
			if (hLine.nMask.x > 0)
			{
				var hTweenClose:TimelineMax = new TimelineMax();
				hTweenClose.append(TweenMax.to(secondInfoBox,.5,{scaleX:0,ease:Sine.easeInOut}));
				hTweenClose.append(TweenMax.to(mainInfoBox.infoLine,.5,{x:235,ease:Sine.easeInOut}), -.1);
				hTweenClose.append(TweenMax.to(mainInfoBox,.5,{scaleX:0,ease:Sine.easeInOut}), -.1);
				hTweenClose.append(TweenMax.to(hLine.nMask,.5,{x:-210,ease:Sine.easeInOut}), -.1);
				hTweenClose.append(TweenMax.to(searchHometown.bg.bgWhite,.5,{alpha:0,ease:Sine.easeInOut, onComplete:checkFinish, onCompleteParams:[tempValue]}));
			}*/
			checkFinish(tempValue);
		}
		
		private function checkFinish(currentValue:String):void
		{
			mainInfoBox.infoLine.visible = false;
			buildNameScreen(currentValue);
			clearBottomScreen();
		}

		private function buildNameScreen(athleteName:String):void
		{
			finalNameSet = false;
			isClicked = false;
			
			//////create the new display and load the choasen athlete//////
			upperSearchBG = new UpperSearchBG;
			myStage.addChild(upperSearchBG);
			upperSearchBG.x = xOffset;
			
			//trace("athleteName = " + athleteName);
			
			for (var i:int = 0; i < athletes.length; i++)
			{
				//trace("displayName = " + athletes[i].aDisplayName);
				//athleteName = buildDisplayName(athletes[i]);
				if (athletes[i].aDisplayName == athleteName)
				{
					upperSearchBG.loadAthlete(athletes[i]);
					break;
				}
			}
			
			//////Set up the back button//////
			back_btn = new BackButton();
			myStage.addChild(back_btn);
			back_btn.x = xOffset + xLowerOffset-60;
			back_btn.y = 2640;
			back_btn.addEventListener(MouseEvent.CLICK, goBack);
		}
		
		private function buildDisplayName(tempAthlete:Athlete):String
		{
			var workLength:Number = 20 - tempAthlete.lastName.length;
			var nameBeginning:String = "";
			var useBeginning:String;
			var tempName:String;
			
			if (tempAthlete.fullName.length > 20)
			{
				if (tempAthlete.maidenName != null)
				{
					nameBeginning = tempAthlete.firstName + " " + tempAthlete.maidenName;
				}
				else
				{
					nameBeginning = tempAthlete.firstName;
				}
			}
			
			if (nameBeginning.length > workLength)
			{
				useBeginning = nameBeginning.substr(0, (workLength - 4));
				tempName = useBeginning + "... " + tempAthlete.lastName;
			}
			else
			{
				tempName = tempAthlete.fullName;
			}
			
			return tempName;
		}

		private function goBack(e:MouseEvent):void
		{
			//////turn on main screen assets//////
			isClicked = false;
			searchName.visible = true;
			searchHometown.visible = true;
			searchAlmaMater.visible = true;
			searchSport.visible = true;
			searchName.addEventListener(MouseEvent.CLICK, nameSearch);
			searchSport.addEventListener(MouseEvent.CLICK, sportSearch);
			searchAlmaMater.addEventListener(MouseEvent.CLICK, almaMaterSearch);
			searchHometown.addEventListener(MouseEvent.CLICK, homeTownSearch);
			var infoArray:Array = new Array();
			
			if (mainInfoBox != null)
			{
				mainInfoBox.visible = true;
				hLine.visible = true;
				nLine.visible = true;
				sLine.visible = true;
				aLine.visible = true;
			}
			
			if (secondInfoBox != null)
			{
				checkSecondBox();
				mainInfoBox.infoLine.visible = true;
				setCurrentValue(chosenCategory);
			}
			
			if (alphabetSliderBar != null)
			{
				if (whichButton == "Names")
				{
					alphabetSliderBar.visible = true;
				}
				else
				{
					alphabetSliderBar.visible = false;
				}
			}
			
			
			upperSearchBG.visible = false;
			back_btn.visible = false;
			helperText.visible = true;
			
			//////Add the intro button tweens. First one delayed and the rest staggered//////
			/*var tweenClose:TimelineMax = new TimelineMax();
			tweenClose.append(TweenMax.from(searchName.bg, 1, {frame:1, frame:25, ease:Linear.easeNone}), .2);
			tweenClose.append(TweenMax.from(searchSport.bg, 1, {frame:1, frame:25, ease:Linear.easeNone}), -.75);
			tweenClose.append(TweenMax.from(searchAlmaMater.bg, 1, {frame:1, frame:25, ease:Linear.easeNone}), -.75);
			tweenClose.append(TweenMax.from(searchHometown.bg, 1, {frame:1, frame:25, ease:Linear.easeNone, onComplete:introFinish}), -.75);*/
			
			//////reset the main attract loop video//////
			//attractNC.connect(null);
			//attractNS = new NetStream(attractNC);
			//attractLoop.attachNetStream(attractNS);
			//attractNS.client = {onMetaData:ns_onMetaData,NetStatusEvent:ns_onPlayStatus};
			attractNS.play("lsmAttractloop.f4v");
			attractOn = true;
			//attractNS.addEventListener(NetStatusEvent.NET_STATUS, ns_onPlayStatus);
		}

		private function clearBottomScreen():void
		{
			//////hide all main screen assets//////
			if (whichButton == "Names")
			{
				alphabetSliderBar.visible = false;
				isClicked = false;
			}

			searchName.visible = false;
			searchHometown.visible = false;
			searchAlmaMater.visible = false;
			searchSport.visible = false;
			searchName.removeEventListener(MouseEvent.CLICK, nameSearch);
			searchSport.removeEventListener(MouseEvent.CLICK, sportSearch);
			searchAlmaMater.removeEventListener(MouseEvent.CLICK, almaMaterSearch);
			searchHometown.removeEventListener(MouseEvent.CLICK, homeTownSearch);
			
			//////remove the secondary and main box//////
			if (secondInfoBox != null)
			{
				mainInfoBox.infoLine.visible = false;
				secondInfoBox.infoLine.visible = false;
				secondInfoBox.visible = false;
			}
			
			if (mainInfoBox != null)
			{
				mainInfoBox.visible = false;
				hLine.visible = false;
				nLine.visible = false;
				sLine.visible = false;
				aLine.visible = false;
			}
			//checkSecondBox();
			//checkMainBox();
		}
		
		public function resetTimeout(e:MouseEvent = null):void
		{
			timeoutTimer.stop();
			timeoutTimer.reset();
			timeoutTimer.start();
			myStage.removeEventListener(MouseEvent.CLICK, resetTimeout);
			myStage.addEventListener(MouseEvent.CLICK, resetTimeout);
		}
		private function onTimeout(e:TimerEvent):void
		{
			myStage.removeEventListener(MouseEvent.MOUSE_DOWN, resetTimeout);
			timeoutTimer.stop();
			timeoutTimer.reset();
			
			//////turn on main screen assets//////
			isClicked = false;
			searchName.visible = true;
			searchHometown.visible = true;
			searchAlmaMater.visible = true;
			searchSport.visible = true;
			helperText.visible = true;
			helperText.gotoAndStop(1);
			
			//////Add the intro button tweens. First one delayed and the rest staggered//////
			searchName.removeEventListener(MouseEvent.CLICK, nameSearch);
			searchSport.removeEventListener(MouseEvent.CLICK, sportSearch);
			searchAlmaMater.removeEventListener(MouseEvent.CLICK, almaMaterSearch);
			searchHometown.removeEventListener(MouseEvent.CLICK, homeTownSearch);
			var tweenClose:TimelineMax = new TimelineMax();
			tweenClose.append(TweenMax.from(searchName.bg, 1, {frame:1, frame:25, ease:Linear.easeNone}), .2);
			tweenClose.append(TweenMax.from(searchSport.bg, 1, {frame:1, frame:25, ease:Linear.easeNone}), -.75);
			tweenClose.append(TweenMax.from(searchAlmaMater.bg, 1, {frame:1, frame:25, ease:Linear.easeNone}), -.75);
			tweenClose.append(TweenMax.from(searchHometown.bg, 1, {frame:1, frame:25, ease:Linear.easeNone, onComplete:introFinish}), -.75);

			//////remove the secondary and main box//////
			if (secondInfoBox != null)
			{
				myStage.removeChild(secondInfoBox);
				mainInfoBox.infoLine.visible = false;
				secondInfoBox.infoLine.visible = false;
				secondInfoBox = null;
			}
			
			if (mainInfoBox != null)
			{
				nLine.nMask.x = -210;
				sLine.nMask.x = -210;
				aLine.nMask.x = -210;
				hLine.nMask.x = -210;
				
				mainInfoBox.infoLine.x = 235;
				
				hLine.visible = false;
				nLine.visible = false;
				sLine.visible = false;
				aLine.visible = false;
				nLine.lineBlur.visible = false;
				hLine.lineBlur.visible = false;
				sLine.lineBlur.visible = false;
				aLine.lineBlur.visible = false;
				aLine.lineBlur.gotoAndStop(1);
				hLine.lineBlur.gotoAndStop(1);
				sLine.lineBlur.gotoAndStop(1);
				nLine.lineBlur.gotoAndStop(1);
				
				myStage.removeChild(mainInfoBox);
				mainInfoBox = null;
			}
			if (upperSearchBG != null)
			{
				upperSearchBG.visible = false;
				back_btn.visible = false;
			}
			if (alphabetSliderBar != null)
			{
				alphabetSliderBar.visible = false;
			}
			
			attractNS.play("lsmAttractloop.f4v");
			attractOn = true;
		}

		private function buildHomeTownArray():void
		{
			//////build array for each hometown and sort//////
			for (var j = 0; j < athletes.length; j++)
			{
				var isDuplicate:Boolean = false;
				for (var k = 0; k < homeTowns.length; k++)
				{
					if (athletes[j].homeTown == homeTowns[k])
					{
						isDuplicate = true;
					}
				}

				if (isDuplicate == false)
				{
					homeTowns.push(athletes[j].homeTown);
				}
			}
			homeTowns.sort();
		}

		private function buildAlmaMaterArray():void
		{
			//////build array for each alma mater and sort//////
			for (var j = 0; j < athletes.length; j++)
			{
				for (var i = 0; i < athletes[j].almaMaterArray.length; i++)
				{
					var isDuplicate:Boolean = false;
					for (var k = 0; k < almaMaters.length; k++)
					{
						if (athletes[j].almaMaterArray[i] == almaMaters[k])
						{
							isDuplicate = true;
						}
					}

					if (isDuplicate == false)
					{
						almaMaters.push(athletes[j].almaMaterArray[i]);
					}
				}
			}
			almaMaters.sort();
		}

		private function buildCategoriesArray():void
		{
			//////build array for each category and sort//////
			for (var j = 0; j < athletes.length; j++)
			{
				for (var i = 0; i < athletes[j].categoriesArray.length; i++)
				{
					var isDuplicate:Boolean = false;
					for (var k = 0; k < categories.length; k++)
					{
						if (athletes[j].categoriesArray[i] == categories[k])
						{
							isDuplicate = true;
						}
					}
					if (isDuplicate == false)
					{
						categories.push(athletes[j].categoriesArray[i]);
					}
				}
			}
			categories.sort();
		}
	}
}