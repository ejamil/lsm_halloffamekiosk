﻿package  code
{
	import flash.display.MovieClip;
	import flash.text.TextField;
	import flash.xml.*;
	import flash.events.Event;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Loader;
	import flash.events.ProgressEvent;
	import flash.net.URLRequest;
	
	public class Athlete extends MovieClip
	{
		public var firstName:String;
		public var maidenName:String;
		public var lastName:String;
		public var categories:String;
		public var categoriesArray:Array = new Array();
		public var categoriesXML:XMLList;
		public var sportNameLeague:String;
		public var sportNameLeagueArray:Array = new Array();
		public var sportNameLeagueXML:XMLList;
		public var yearInducted:String;
		public var nicknames:String;
		public var nicknamesArray:Array = new Array();
		public var nicknamesXML:XMLList;
		public var homeTown:String;
		public var birth:String;
		public var death:String;
		public var almaMater:String;
		public var almaMaterArray:Array = new Array();
		public var almaMaterXML:XMLList;
		public var louisiannaTies:String;
		public var biographyAchievements:String;
		public var notableStatistics:String;
		public var otherHallsofFame:String;
		public var otherHallsofFameArray:Array = new Array();
		public var otherHallsofFameXML:XMLList;
		public var quotes:String;
		public var quotesArray:Array = new Array();
		public var quotesXML:XMLList;
		public var officialPortrait:String;
		public var otherPhotos:String;
		public var otherPhotoArray:Array = new Array();
		public var otherCaptionArray:Array = new Array();
		public var otherCopyrightArray:Array = new Array();
		public var innerOtherPhotosArray:Array;
		public var tempPhotoArray:Array = new Array();
		public var copyrightArray:Array = new Array();
		public var captionArray:Array = new Array();
		public var tempVideoArray:Array = new Array();
		public var vidCopyrightArray:Array = new Array();
		public var vidCaptionArray:Array = new Array();
		public var tempAudioArray:Array = new Array();
		public var audioCopyrightArray:Array = new Array();
		public var audioCaptionArray:Array = new Array();
		public var testPhotosOfArtifactsArray:Array = new Array();
		public var otherPhotosXML:XMLList;
		public var photosOfArtifacts:String;
		public var photoArray:Array = new Array();
		public var innerPhotosOfArtifactsArray:Array;
		public var photosOfArtifactsXML:XMLList;
		public var videos:String;
		public var videosArray:Array = new Array();
		public var innerVideoArray:Array;
		public var videosXML:XMLList;
		public var audio:String;
		public var audioArray:Array = new Array();
		public var innerAudioArray:Array;
		public var audioXML:XMLList;
		public var bitmapData:BitmapData;
		public var bmVis:Bitmap;
		
		public var aDisplayName:String;
		
		public var fullName:String;
		
		private var nameString:String;
		
		public var athlete_txt:TextField;
		
		public function Athlete(efirstName:String,
								emaidenName:String,
								elastName:String,
								ecategories:String,
								esportsNameLeague:String,
								eyearInducted:String,
								enicknames:String,
								ehomeTown:String,
								ebirth:String,
								edeath:String,
								ealmaMater:String,
								elouisiannaTies:String,
								ebiographyAchievements:String,
								enotableStatistics:String,
								eotherHallsofFame:String,
								equotes:String,
								eofficialPortrait:String,
								eotherPhotos:String,
								ephotosofArtifacts:String,
								evideos:String,
								eaudio:String) 
		{			
			firstName = efirstName;
			maidenName = emaidenName;
			lastName = elastName;
			categories = ecategories;
			sportNameLeague = esportsNameLeague;
			yearInducted = eyearInducted;
			nicknames = enicknames;
			homeTown = ehomeTown;
			birth = ebirth;
			death = edeath;
			almaMater = ealmaMater;
			louisiannaTies = elouisiannaTies;
			biographyAchievements = ebiographyAchievements;
			notableStatistics = enotableStatistics;
			otherHallsofFame = eotherHallsofFame;
			quotes = equotes;
			officialPortrait = eofficialPortrait;
			otherPhotos = eotherPhotos;
			photosOfArtifacts = ephotosofArtifacts;
			videos = evideos;
			audio = eaudio;
			
			nameString = firstName + " " + lastName;
			buildCategories();
			buildNicknames();
			buildSportNameLeague();
			buildAlmaMaters();
			buildHallsofFame();
			buildQuotes();
			buildPhotos();
			buildArtifacts();
			buildVideos();
			buildAudio();
			buildFullName();
			buildDisplayName();
		}
		
		private function buildCategories():void
		{
			categoriesXML = new XMLList(categories);
			for (var i = 0; i < categoriesXML.Category.length(); i++)
			{
				categoriesArray.push(categoriesXML.Category[i]);
			}
		}
		
		private function buildNicknames():void
		{
			nicknamesXML = new XMLList(nicknames);
			for (var i = 0; i < nicknamesXML.NickName.length(); i++)
			{
				nicknamesArray.push(nicknamesXML.Nickname[i]);
			}
		}
		
		private function buildSportNameLeague():void
		{
			sportNameLeagueXML = new XMLList(sportNameLeague);
			for (var i = 0; i < sportNameLeagueXML.Sports.length(); i++)
			{
				sportNameLeagueArray.push(sportNameLeagueXML.Sports[i]);
			}
		}
		
		private function buildAlmaMaters():void
		{
			almaMaterXML = new XMLList(almaMater);
			for (var i = 0; i < almaMaterXML.Alma_Mater.length(); i++)
			{
				almaMaterArray.push(almaMaterXML.Alma_Mater[i]);
			}
		}
		
		private function buildHallsofFame():void
		{
			otherHallsofFameXML = new XMLList(otherHallsofFame);
			for (var i = 0; i < otherHallsofFameXML.Other_Halls_of_Fame.length(); i++)
			{
				otherHallsofFameArray.push(otherHallsofFameXML.Other_Halls_of_Fame[i]);
			}
		}
		
		private function buildQuotes():void
		{
			quotesXML = new XMLList(quotes);
			for (var i = 0; i < quotesXML.Quote.length(); i++)
			{
				quotesArray.push(quotesXML.Quote[i]);
			}
		}
		
		private function buildPhotos():void
		{
			otherPhotosXML = new XMLList(otherPhotos);
			for (var i = 0; i < otherPhotosXML.Other_Photo.length(); i++)
			{
				innerOtherPhotosArray = new Array();
				innerOtherPhotosArray.push(otherPhotosXML.Other_Photo[i].Caption);
				innerOtherPhotosArray.push(otherPhotosXML.Other_Photo[i].Copyright);
				innerOtherPhotosArray.push(otherPhotosXML.Other_Photo[i].File);
				otherCaptionArray.push(otherPhotosXML.Other_Photo[i].Caption);
				otherCopyrightArray.push(otherPhotosXML.Other_Photo[i].Copyright);
				otherPhotoArray.push(otherPhotosXML.Other_Photo[i].File);
			}
		}
		
		private function buildArtifacts():void
		{			
			photosOfArtifactsXML = new XMLList(photosOfArtifacts);
			for (var i = 0; i < photosOfArtifactsXML.Photo_for_Inductee.length(); i++)
			{	
				innerPhotosOfArtifactsArray = new Array();
				innerPhotosOfArtifactsArray.push(photosOfArtifactsXML.Photo_for_Inductee[i].Caption);
				innerPhotosOfArtifactsArray.push(photosOfArtifactsXML.Photo_for_Inductee[i].Copyright);
				innerPhotosOfArtifactsArray.push(photosOfArtifactsXML.Photo_for_Inductee[i].File);
				photoArray.push(photosOfArtifactsXML.Photo_for_Inductee[i].File);
				captionArray.push(photosOfArtifactsXML.Photo_for_Inductee[i].Caption);
				copyrightArray.push(photosOfArtifactsXML.Photo_for_Inductee[i].Copyright);
				//photoArray.push(innerPhotosOfArtifactsArray);
			}
		}
		
		private function buildVideos():void
		{
			videosXML = new XMLList(videos);
			for (var i = 0; i < videosXML.Video.length(); i++)
			{
				innerVideoArray = new Array();
				innerVideoArray.push(videosXML.Video[i].Caption);
				innerVideoArray.push(videosXML.Video[i].Copyright);
				innerVideoArray.push(videosXML.Video[i].File);
				videosArray.push(videosXML.Video[i].File);
				vidCaptionArray.push(videosXML.Video[i].Caption);
				vidCopyrightArray.push(videosXML.Video[i].Copyright);
				//videosArray.push(innerVideoArray);
			}
		}
		
		private function buildAudio():void
		{
			audioXML = new XMLList(audio);
			for (var i = 0; i < audioXML.Audio.length(); i++)
			{
				innerAudioArray = new Array();
				innerAudioArray.push(audioXML.Audio[i].Caption);
				innerAudioArray.push(audioXML.Audio[i].Copyright);
				innerAudioArray.push(audioXML.Audio[i].File);
				audioArray.push(audioXML.Audio[i].File);
				audioCaptionArray.push(audioXML.Audio[i].Caption);
				audioCopyrightArray.push(audioXML.Audio[i].Copyright);
				//audioArray.push(innerAudioArray);
			}
		}
		
		private function buildFullName():void
		{
			if (maidenName != "")
			{
				fullName = firstName + " " + maidenName + " " + lastName;
			}
			else
			{
				fullName = firstName + " " + lastName;
			}
		}
		
		private function buildDisplayName():void
		{
			var workLength:Number = 20 - lastName.length;
			var nameBeginning:String = "";
			var useBeginning:String;
			if (fullName.length > 20)
			{
				if (maidenName != null)
				{
					nameBeginning = firstName + " " + maidenName;
				}
				else
				{
					nameBeginning = firstName;
				}
			}
			
			if (nameBeginning.length > workLength)
			{
				useBeginning = nameBeginning.substr(0, (workLength - 4));
				aDisplayName = useBeginning + "... " + lastName;
			}
			else
			{
				aDisplayName = fullName;
			}
		}
		
		public function outputAthlete():void
		{
			
		}

	}
	
}
