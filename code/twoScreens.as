﻿package code
{
	import flash.desktop.NativeApplication;// for exit application
	import flash.display.MovieClip;
	import flash.display.NativeWindowInitOptions;// for Native Window
	import flash.display.NativeWindowSystemChrome;//  "   "       "
	import flash.display.NativeWindowType;//  "   "       " 
	import flash.display.NativeWindow;// for Native Window
	import flash.display.StageAlign;// working with stage of new Native window
	import flash.display.StageScaleMode;//  "       "     "    "  "    "      " 
	import flash.geom.Rectangle;
	import flash.events.Event;
	import flash.media.Video;

	public class twoScreens
	{
		public var mainWindow:NativeWindow;
		public var windowOptions:NativeWindowInitOptions = new NativeWindowInitOptions();
		public var vidList:Array = new Array();
		public var mcList:Array = new Array();

		public function twoScreens(__x:Number, __y:Number,w:Number,h:Number)
		{
			windowOptions.systemChrome = NativeWindowSystemChrome.NONE;// no title bars
			windowOptions.type = NativeWindowType.NORMAL;//Normal window
			mainWindow = new NativeWindow(windowOptions);// apply your options
			mainWindow.stage.scaleMode = StageScaleMode.NO_SCALE;
			mainWindow.stage.align = StageAlign.TOP;
			mainWindow.bounds = new Rectangle(__x,__y,w,h);// loc and size you want window to be
			mainWindow.x = -143;
			mainWindow.y = 0;
			mainWindow.activate();
			mainWindow.addEventListener(Event.CLOSE, quitRoot);
		}

		private function quitRoot(e:Event):void
		{
			mainWindow.removeEventListener(Event.REMOVED, quitRoot);
			NativeApplication.nativeApplication.exit();
		}

		public function addChildToWindow(mc:MovieClip):void
		{
			mainWindow.stage.addChild(mc);
			mcList.push(mc);
		}

		public function addVideoToWindow(vid:Video):void
		{
			mainWindow.stage.addChild(vid);
			vidList.push(vid);
		}

		public function removeChildFromWindow(mc:MovieClip):void
		{
			if (mainWindow.stage.contains(mc))
			{
				mainWindow.stage.removeChild(mc);
			}
		}
		
		public function removeVideoFromWindow(vid:Video):void
		{
			if (mainWindow.stage.contains(vid))
			{
				mainWindow.stage.removeChild(vid);
			}
		}

		public function checkPresent(mc:MovieClip):Boolean
		{
			if (mainWindow.stage.contains(mc))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		public function clearAll():void
		{
			var i:Number;
			if (vidList.length > 0)
			{

				for (i=0; i<vidList.length; i++)
				{
					//trace(" remove v= "+ vidList[i].name);
					if (mainWindow.stage.contains(vidList[i]))
					{
						mainWindow.stage.removeChild(vidList[i]);
					}
				}
			}
			if (mcList.length > 0)
			{
				for (i=0; i<mcList.length; i++)
				{
					if (mainWindow.stage.contains(mcList[i]))
					{
						//trace("m= " + mcList[i]);
						mainWindow.stage.removeChild(mcList[i]);
					}
				}
			}
		}

		public function checkVidPresent(vid:Video):Boolean
		{
			if (mainWindow.stage.contains(vid))
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public function whatsThere():void
		{
			trace("vids in mainWindow= " + vidList);
			trace("mcs in mainWindow= " + vidList);
		}
	}
}