﻿package code
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.ProgressEvent;
	import flash.events.IOErrorEvent;
	import flash.net.URLRequest;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.filters.*;
	import flash.geom.Point;
	import flash.geom.Rectangle;

	import gs.*;
	import gs.easing.*;
	import code.UpperSearchBG;

	public class AudioButton extends MovieClip
	{
		private var menuAudio:Array = new Array();
		private var thumbArray:Array = new Array();
		private var captionArray:Array = new Array();
		private var copyrightArray:Array = new Array();
		public var next_btn:NextButton;
		public var prev_btn:PrevButton;
		private var yButtonLoc:Number = 102;
		private var thumbHolder:Sprite = new Sprite; //add thum images in to this Sprite
		private var waveHolder:Sprite = new Sprite; //add waveHolder image in to the container
		public var thumbContainer:Sprite = new Sprite; //add thumbHolder images in to the container
		private var noofAud:int; //Number of videos 
		private var space:int = 100;
		private var owner:UpperSearchBG;
		private var boxWidth:Number;
		private var boxHeight:Number;
		public var currentIndex:Number;
		private var glowArray:Array = [new GlowFilter()];

		private var controls:VidControls;
		private var audioPath:String;
		private var myChannel:SoundChannel;
		private var audioToggle:Boolean;
		private var p:uint = 0;
		private var preloader:Preloader;
		private var mySound:Sound;
		private var soundImage:WaveImg;

		public function AudioButton(_menuAudio:Array, _captionsArray:Array, _copyrightArray:Array, needsButtons:Boolean, myOwner:UpperSearchBG):void
		{
			//////Reference to the parent class//////
			owner = myOwner;

			//////Set glow filter properties////// 
			glowArray[0].color = 0xFFFF00;
			glowArray[0].blurX = 15;
			glowArray[0].blurY = 15;

			//////Adds next and previous buttons and sets their properties//////
			if (needsButtons = true)
			{
				prev_btn = new PrevButton();
				this.addChild(prev_btn);
				prev_btn.x = 120;
				prev_btn.y = yButtonLoc;
				prev_btn.addEventListener(MouseEvent.CLICK,goBackward);

				next_btn = new NextButton();
				this.addChild(next_btn);
				next_btn.x = this.width - 75;
				next_btn.y = yButtonLoc;
				next_btn.addEventListener(MouseEvent.CLICK,goForward);
			}
			prev_btn.visible = false;
			next_btn.visible = false;

			//////Set max width and height of the top audio//////
			boxWidth = owner.upperInfo2.width;
			boxHeight = owner.upperInfo2.height;

			//////Populate Arrays//////
			menuAudio = _menuAudio;
			captionArray = _captionsArray;
			copyrightArray = _copyrightArray;

			noofAud = menuAudio.length;//get number of audios
			thumbContainer.mask = thumbMask;//set mask on thumbnails
			
			preloader = new Preloader();
			p = 0;
			myChannel = new SoundChannel();
			mySound = new Sound();
			myChannel.stop();
			
			//////Center waveBar and show the appropriate caption and copyright text//////
			waveHolder.x = 0;//resets the large image X position
			waveHolder.x = owner.upperInfo2.width / 2 - waveHolder.width / 2;
			waveHolder.y = owner.upperInfo2.imagePlacer.y + owner.upperInfo2.imagePlacer.height/2 - waveHolder.height / 2;
			owner.upperInfo2.addChild(waveHolder);
			soundImage = new WaveImg();
		}
    
		function soundProgress(event:Event):void 
		{   
			//////Finds the length of the song then scales the progress bar to the % thats been played//////
			var loadTime:Number = mySound.bytesLoaded / mySound.bytesTotal;
			var loadPercent:uint = Math.round(100 * loadTime);
			var estimatedLength:int = Math.ceil(mySound.length / (loadTime));
			var playbackPercent:uint = Math.round(100 * (myChannel.position / estimatedLength));
			soundImage.progressBar.scaleX = playbackPercent/100;
		}

		private function progressHandler(event:ProgressEvent):void 
		{
			//////Set up preloader math for how much of the audio is loaded//////
			var fbl:Number = event.bytesLoaded;
   			var fbt:Number = event.bytesTotal; 
			var fper:Number = Math.floor(fbl/fbt*100);
			preloader.percentText.text = fper + "%";
			preloader.percentBar.scaleX = fper/100;
				
			//////If audio is fully loaded, remove preloader and play the audio//////
			if (fper >= 100 && audioPath != "") {
				preloader.percentText.text = "";
				owner.upperInfo2.removeChild(preloader);
    		}
		}
		
		function onLoadComplete(event:Event):void 
		{ 
			//////If audio is fully loaded, play the audio//////
			myChannel = mySound.play(); 
			myChannel.addEventListener(Event.SOUND_COMPLETE, soundCompleteHandler);
			waveHolder.addChild(soundImage);
			addEventListener(Event.ENTER_FRAME, soundProgress);
		}

		public function activateThumbs():void
		{
			//////Create audio controls//////
			controls = new VidControls();
			this.addChild(controls);
			controls.x = 277;
			controls.y = yButtonLoc;
			controls.playBtn.visible = false;

			//////Sets Index to 0 and displays the first image//////
			currentIndex = 0;
			var ci:int = int(currentIndex);
			loadAudio(ci);
			
			//////makes sure the buttons are visible and item counter text is correct//////
			prev_btn.alpha = 1;
			next_btn.alpha = 1;
			owner.contDisplay.visible = true;
			owner.contDisplay.contNum.text = String(currentIndex+1);
			owner.contDisplay.outOf.text = String(noofAud);

			//////Loads each thumb into the gallery//////
			for (var i:Number = 0; i < noofAud; i++)
			{
				//////Sets the initial thumbnail properties and adds them to array and stage//////
				var thumbC:AudThumb = new AudThumb();
				thumbContainer.x = thumbMask.x - 24;
				thumbContainer.y = thumbMask.y - 5;
				thumbC.audNum.text = String(i+1);
				thumbArray.push(thumbC);
				thumbContainer.addChild(thumbC);
				addChild(thumbContainer);
			}
			
			//////add listeners and modes//////
			prev_btn.mouseEnabled = true;
			next_btn.mouseEnabled = true;
			prev_btn.buttonMode = true;
			next_btn.buttonMode = true;
			controls.buttonMode = true;
			controls.addEventListener(MouseEvent.CLICK,controlClick);
			prev_btn.addEventListener(MouseEvent.CLICK,goBackward);
			next_btn.addEventListener(MouseEvent.CLICK,goForward);
			this.addEventListener(Event.ENTER_FRAME, init);
		}

		public function deactivateThumbs():void
		{
			//////Clean out previous image and audio//////
			while (waveHolder.numChildren > 0)
			{
				waveHolder.removeChildAt(0);
				controls.buttonMode = false;
				controls.removeEventListener(MouseEvent.CLICK,controlClick);
				this.removeChild(controls);
				removeEventListener(Event.ENTER_FRAME, soundProgress);
			}
			myChannel.stop();
			
			//////Removes all thumbs and clears thumbArray//////
			owner.contDisplay.visible = false;
			var len:int = thumbArray.length;
			for (var i:int = 0; i < len; i++)
			{
				thumbContainer.removeChild(thumbArray[i]);
			}
			thumbArray.length = 0;

			//////Removes all audio, controls and text. Does not remove arrays and stored information//////
			owner.upperInfo2.captionText.text = "";
			owner.upperInfo2.copyrightText.text = "";

			//////remove listeners and modes//////
			prev_btn.visible = false;
			next_btn.visible = false;
			prev_btn.mouseEnabled = false;
			next_btn.mouseEnabled = false;
			prev_btn.buttonMode = false;
			next_btn.buttonMode = false;
			prev_btn.removeEventListener(MouseEvent.CLICK,goBackward);
			next_btn.removeEventListener(MouseEvent.CLICK,goForward);
		}
		private function init(event:Event):void
		{				
			//////Hides and shows the buttons depending on thumbnail positions//////
			if (currentIndex <= 0)
			{
				prev_btn.visible = false;
			}
			else
			{
				prev_btn.visible = true;
			}
			if (currentIndex >= noofAud -1)
			{
				next_btn.visible = false;
			}
			else
			{
				next_btn.visible = true;
			}
			if (prev_btn.alpha == 0)
			{
				prev_btn.visible = false;
				next_btn.visible = false;
			}

			//////Prevents looping and sets the thumbnail X and Y proportionally to the adjacent thumbnail//////
			if (thumbArray.length > 0)
			{
				for (var i:Number = 0; i < noofAud; i++)
				{
					if (i==0)
					{
						thumbArray[i].x = 0;
					}
					else
					{
						thumbArray[i].x = thumbArray[i - 1].x + thumbArray[i - 1].width + 25;
					}

					thumbArray[i].y =  -  thumbArray[i].height / 2;

					/////////If no rollOver, highlight the thumb right in the middle////////
					if (currentIndex == i)
					{
						thumbArray[i].filters = glowArray;
					}
					else
					{
						thumbArray[i].filters = [];
					}
				}
			}
		}

		public function loadAudio(i:int):void
		{
			//////Clean out previous image and audio//////
			while (waveHolder.numChildren > 0)
			{
				addEventListener(Event.ENTER_FRAME, soundProgress);
				waveHolder.removeChildAt(0);
			}
			myChannel.stop();
			
			//////Creates the preloader//////
			owner.upperInfo2.addChild(preloader);
			preloader.x = owner.upperInfo2.width / 2;
			preloader.y = owner.upperInfo2.imagePlacer.y + owner.upperInfo2.imagePlacer.height / 2;

			//////Creates new audio and adds it to the top box//////						
			audioPath = String(menuAudio[i]);
			mySound = new Sound();
			mySound.load(new URLRequest(audioPath));
 		    mySound.addEventListener(ProgressEvent.PROGRESS, progressHandler); 
			mySound.addEventListener(Event.COMPLETE, onLoadComplete);
			myChannel.addEventListener(Event.SOUND_COMPLETE, soundCompleteHandler);
			
			//////Shows appropriate controls button//////
			controls.playBtn.visible = false;
			controls.pauseBtn.visible = true;
			audioToggle = true;
			
			//////Shows appropriate captions and copyright//////
			owner.upperInfo2.captionText.text = captionArray[i];
			owner.upperInfo2.copyrightText.text = copyrightArray[i];
		}

		private function controlClick(event:MouseEvent):void
		{
			//////when clicked if playing, hide play btn and pause//////
			if (audioToggle == true)
			{
				audioToggle = false;
				p = Math.floor(myChannel.position);
				myChannel.stop();
				controls.playBtn.visible = true;
				controls.pauseBtn.visible = false;
			}
			//////when clicked if paused, hide pause btn and play//////
			else if (audioToggle == false)
			{
				audioToggle = true;
				myChannel = mySound.play(p);
				controls.pauseBtn.visible = true;
				controls.playBtn.visible = false;
			}
		}
		
		private function soundCompleteHandler(e:Event):void {
			//////when the sound ends, reset to the beginning and stop//////
            p = 0;
			myChannel = mySound.play(0);
			controls.pauseBtn.visible = false;
			controls.playBtn.visible = true;
			myChannel.stop();
			audioToggle = false;
        }
		
		private function goForward(event:MouseEvent):void
		{
			//////Slides thumbs to the left. Prevents looping. Then calls to make large video = to the current thumb//////
			if (currentIndex < noofAud)
			{
				currentIndex++;
				owner.contDisplay.contNum.text = String(currentIndex+1);
				var diff:Number = thumbContainer.x - 56 - 20;
				TweenNano.to(thumbContainer,0.2,{x:diff,ease:Strong.easeOut});
			}
			var i:int = int(currentIndex);
			loadAudio(i);
		}

		private function goBackward(event:MouseEvent):void
		{
			//////Slides thumbs to the right. Prevents looping. Then calls to make large video = to the current thumb//////
			if (currentIndex > 0)
			{
				currentIndex--;
				owner.contDisplay.contNum.text = String(currentIndex+1);
				var diff:Number = thumbContainer.x + 56 + 20;
				TweenNano.to(thumbContainer,0.2,{x:diff,ease:Strong.easeOut});
			}
			var i:int = int(currentIndex);
			loadAudio(i);
		}
	}
}