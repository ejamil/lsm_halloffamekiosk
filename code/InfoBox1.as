﻿package code
{

	import flash.display.Sprite;
	import flash.display.MovieClip;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.display.Shape;
	import flash.text.TextFormat;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.Font;
	import gs.*;
	import gs.easing.*;
	import com.tutsplus.active.util.Align;
	import flash.geom.Rectangle;

	public class InfoBox extends MovieClip
	{

		private var _container:MovieClip;
		private var _item:Item;
		public var _itemTextField:TextField;
		private var _defaultFormat:TextFormat = new TextFormat();
		private var _proximaNova:Font = new ProximaNovaAltCondReg();
		private var _textFieldXPosition:uint = 43;
		private var _textFieldYPosition:uint = 0;
		private var _textFieldWidth:uint = 242;
		private var _textFieldHeight:uint = 36;
		private var _itemPosition:Number = -115;
		private var _mask:Shape;
		private var _maskWidth:uint = 284;
		private var _maskHeight:uint = 462;
		private var _paddingTop:Number = 120;
		private var _background:Shape;
		private var _maxSpeed:uint = 100;
		private var _speed:Number;
		private var _items:Array = new Array();
		private var _stage:MovieClip;
		public var scrollModel:Array;

		private var _bounds:Rectangle;
		private var _scrolling:Boolean = false;
		private var _infoSlider:InfoSlider;
		private var contentRange:Number;

		private var owner:Document;

		private var secondBox:Boolean = false;

		public function InfoBox(infoArray:Array, myStage:MovieClip, myOwner:Document, isSecondBox:Boolean = false)
		{
			owner = myOwner;

			secondBox = isSecondBox;

			this.scrollModel = infoArray;
			this._stage = myStage;
			createRollingScroller();

			_infoSlider = new InfoSlider();
			this.addChild(_infoSlider);
			_bounds = new Rectangle(290,4,0,_maskHeight - _infoSlider.height + 2);
			_infoSlider.x = 290;
			_infoSlider.y = 4;
			TweenMax.to(_infoSlider,.3,{delay:.5, y:4,ease:Sine.easeInOut});
			_infoSlider.addEventListener(MouseEvent.MOUSE_DOWN, startScroll);
			_stage.addEventListener(MouseEvent.MOUSE_UP, stopScroll);
			this.addEventListener(Event.ENTER_FRAME, updateScroll);
		}

		private function startScroll(e:MouseEvent):void
		{
			_scrolling = true;
			_infoSlider.startDrag(false, _bounds);
		}

		private function stopScroll(e:MouseEvent):void
		{
			_scrolling = false;
			_infoSlider.stopDrag();
		}

		private function updateScroll(e:Event):void
		{
			if (_scrolling == true)
			{
				var scrollPercent = (Math.abs((_infoSlider.y + 116) - _mask.y))/_mask.height;
				var _containerY = _mask.y - (scrollPercent * contentRange);

				var newY:Number = (((_mask.height-_container.height)*(_mask.y-_infoSlider.y))
				/
				(_mask.height-_infoSlider.height)) + 1740;
				TweenMax.to(_container, .6, {y:_containerY, ease:Cubic.easeOut});
			}
		}

		/*public function scaleTextToFitInTextField( txt : TextField, txtWidth:Number ):void
		{
			var maxTextWidth:int = txtWidth - 5;
			var maxTextHeight:int = 80;

			var f:TextFormat = txt.getTextFormat();

			//decrease font size until the text fits
			while (txt.textWidth > maxTextWidth || txt.textHeight > maxTextHeight)
			{
				f.size = int(f.size) - 1;
				txt.setTextFormat(f);
			}
		}*/

		private function createRollingScroller():void
		{
			_container = new MovieClip();
			this.addChild(_container);
			_container.removeEventListener(MouseEvent.CLICK, sendValue);

			for (var i = 0; i < scrollModel.length; i++)
			{
				_item = new Item();

				_itemTextField = new TextField();
				_itemTextField.x = _textFieldXPosition;
				_itemTextField.y = _textFieldYPosition;
				_itemTextField.selectable = false;
				_itemTextField.width = _textFieldWidth;
				_itemTextField.height = _textFieldHeight;
				_itemTextField.embedFonts = true;
				//_itemTextField.autoSize = "left";

				_defaultFormat.color = 0xffac37;
				_defaultFormat.font = _proximaNova.fontName;
				_defaultFormat.size = 28;

				_itemTextField.defaultTextFormat = _defaultFormat;
				_itemTextField.text = scrollModel[i];				

				_item.addChild(_itemTextField);
				_item.y = _itemPosition + 36 * i;
				_item.buttonMode = true;
				_item.mouseChildren = false;
				_container.addEventListener(MouseEvent.CLICK, sendValue);
				_container.addChild(_item);
			}

			_mask = new Shape();
			_mask.graphics.beginFill(0xFF0000);
			_mask.graphics.drawRect(2, _itemPosition, _maskWidth, _maskHeight);
			_mask.graphics.endFill();
			_mask.y = _paddingTop;
			addChild(_mask);

			_container.mask = _mask;
			_container.y = _paddingTop;

			contentRange = _container.height - _mask.height;
		}

		public function reloadList(infoArray:Array):void
		{
			_infoSlider.y = 25;
			_container.y = _paddingTop + 21;
			_container.removeEventListener(MouseEvent.CLICK, sendValue);
			TweenMax.to(_infoSlider,.5,{delay:.5, y:4,ease:Sine.easeInOut});
			TweenMax.to(_container,.5,{delay:.5, y:_paddingTop,ease:Sine.easeInOut});
	
			if (_container.numChildren > 0)
			{
				for (var j:int = _container.numChildren - 1; j >= 0; j--)
				{
					_container.removeChildAt(j);
				}
			}
			for (var i = 0; i < infoArray.length; i++)
			{
				_item = new Item();

				_itemTextField = new TextField();
				_itemTextField.x = _textFieldXPosition;
				_itemTextField.y = _textFieldYPosition;
				_itemTextField.selectable = false;
				_itemTextField.width = _textFieldWidth;
				_itemTextField.height = _textFieldHeight;
				_itemTextField.embedFonts = true;
				//_itemTextField.autoSize = TextFieldAutoSize.LEFT;

				_defaultFormat.color = 0xffac37;
				_defaultFormat.font = _proximaNova.fontName;
				_defaultFormat.size = 28;

				_itemTextField.defaultTextFormat = _defaultFormat;
				_itemTextField.text = infoArray[i];

				_item.addChild(_itemTextField);
				_item.y = _itemPosition + 36 * i;
				_item.buttonMode = true;
				_item.mouseChildren = false;
				_container.addEventListener(MouseEvent.CLICK, sendValue);
				_container.addChild(_itemTextField);
			}
			contentRange = _container.height - _mask.height;
		}

		private function sendValue(e:MouseEvent):void
		{
			var currentValue:String;
			if (e.target is Item)
			{
				var tempTextField:TextField;
				tempTextField = e.target.getChildAt(1);
				currentValue = tempTextField.text;
			}

			if (secondBox == true)
			{
				owner.finalNameSet = true;
			}
			owner.setCurrentValue(currentValue);
		}

		/*private function movingOver (event:MouseEvent):void {
		_container.removeEventListener(MouseEvent.MOUSE_OVER, movingOver);
		addEventListener(Event.ENTER_FRAME, enterFrame);
		//if (event.target is Item) 
		//TweenMax.to(Item(event.target).item_btn_over, .2, {alpha:1});
		}
		
		private function movingOut (event:MouseEvent):void {
		removeEventListener(Event.ENTER_FRAME, enterFrame);
		_container.addEventListener(MouseEvent.MOUSE_OVER, movingOver);
		//if (event.target is Item)
		//TweenMax.to(Item(event.target).item_btn_over, .2, {alpha:0});
		}*/


		/*function enterFrame(event:Event):void
		{
		scaleTextToFitInTextField(_itemTextField, _textFieldWidth);
		}*/
	}
}