﻿package code
{

	import flash.text.TextFormat;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.Font;
	import flash.display.MovieClip;
	import flash.geom.Rectangle;
	import flash.events.MouseEvent;
	import flash.events.Event;
	import flash.filters.GlowFilter;
	import gs.*;
	import gs.easing.*;

	public class AlphabetSliderBar extends MovieClip
	{
		private var container:MovieClip;
		private var item:Item;
		private var letter:TextField;
		private var letterFormat:TextFormat = new TextFormat();
		private var proximaNova:Font = new ProximaNovaAltCondReg();
		private var proximaNovaBold:Font = new ProximaNovaAltCondBold();
		private var _textFieldXPosition:uint = 43;
		private var _textFieldYPosition:uint = 16;
		private var _textFieldWidth:uint = 36;
		private var _textFieldHeight:uint = 36;
		private var _itemPosition:Number = -17;
		private var _stage:MovieClip;
		
		private var letterOffset:Number = 51.76;
		private var currentLetter:String = "N";
		
		private var showLetter:TextField = new TextField();
		private var showLetterFormat:TextFormat = new TextFormat();

		private var _bounds:Rectangle;
		private var _scrolling:Boolean = false;

		private var sliderLoc:Number = -110;
		private var alphabetSlider:AlphabetSlider;
		
		private var alphabetString:String = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		
		private var owner:Document;
		
		public var outline:GlowFilter=new GlowFilter(0xFFFFCC, .7, 3, 3, 2);


		public function AlphabetSliderBar(myStage:MovieClip, myOwner:Document)
		{
			//////Reference to the document class//////
			owner = myOwner;
			
			//////Add this to the stage, set bounds and parameters//////
			this._stage = myStage;
			addAlphabet();
			alphabetSlider = new AlphabetSlider();
			this.addChild(alphabetSlider);
			alphabetSlider.x = 700;
			alphabetSlider.y = this.y - ((alphabetSlider.height - this.height)/2)+6;
			_bounds = new Rectangle(sliderLoc,this.y - ((alphabetSlider.height - this.height)/2)+6,1182-sliderLoc,0);
			alphabetSlider.addEventListener(MouseEvent.MOUSE_DOWN, startScroll);
			_stage.addEventListener(MouseEvent.MOUSE_UP, stopScroll);
			alphabetSlider.addChild(showLetter);
			showLetter.x = alphabetSlider.width/2 - 12;
			showLetter.y = alphabetSlider.y - 60;
			setLetter();
			this.addEventListener(Event.ENTER_FRAME, init);
			TweenMax.to(alphabetSlider, .7, {x:564, ease:Cubic.easeOut});
		}
		private function init(event:Event):void
		{	
			//////checks which letter should be shown//////
			checkLetter();
		}
		
		public function startScroll(e:MouseEvent):void
		{
			//////starts the scrolling//////
			alphabetSlider.startDrag(false, _bounds);
		}

		public function stopScroll(e:MouseEvent):void
		{
			//////stops scroll and checks which letter should be shown and snaps to letter//////
			alphabetSlider.stopDrag();
			checkFinalLetter();
		}
		public function scrollTo(e:MouseEvent):void
		{
			//////Snap the slider to the letter chosen and display that letter//////
			alphabetSlider.x =e.currentTarget.x - alphabetSlider.width/2 + letterOffset;
			checkFinalLetter();
		}
		
		private function checkLetter():void
		{
			//////for each letter, check the current slider position and display that letter//////
			for (var i:int = 0; i < 26; i++)
			{
				if (alphabetSlider.x >= -132 + (letterOffset*i) && alphabetSlider.x <=-89 + (letterOffset*i))
				{
					currentLetter = alphabetString.charAt(i);
				}
			}
			setLetter();
		}
		
		private function checkFinalLetter():void
		{
			//////for each letter, snap the current slider position and display that letter//////
			for (var i:int = 0; i < 26; i++)
			{
				if (alphabetSlider.x >= -132 + (letterOffset*i) && alphabetSlider.x <=-80 + (letterOffset*i))
				{
					alphabetSlider.x = -110 + (letterOffset * i);
					currentLetter = alphabetString.charAt(i);
				}
			}
			setLetter();
		}
		
		private function setLetter():void
		{
			//////determine which letter is being shown and display it above the slider//////
			showLetter.selectable = false;
			showLetter.width = 48;
			showLetter.height = 48;
			showLetter.embedFonts = true;
			
			showLetterFormat.color = 0xFFFFFF;
			showLetterFormat.font = proximaNovaBold.fontName;
			showLetterFormat.size = 36;

			showLetter.defaultTextFormat = showLetterFormat;
			showLetter.text = currentLetter;
			owner.setLetter(currentLetter);
		}
		
		public function getLetter():String
		{
			//////returns whatever the current letter is//////
			return currentLetter;
		}

		private function addAlphabet():void
		{
			container = new MovieClip();
			this.addChild(container);
			outline.quality=2;
			
			//////for each letter create them and display them along the entire slider bar//////
			for (var i = 0; i < 26; i++)
			{
				item = new Item();
				item.addEventListener(MouseEvent.CLICK, scrollTo);
				item.textBG.visible = false;

				letter = new TextField();
				letter.x = _textFieldXPosition;
				letter.y = _textFieldYPosition;
				letter.selectable = false;
				letter.width = _textFieldWidth;
				letter.height = _textFieldHeight;
				letter.embedFonts = true;

				letterFormat.color = 0x000000;
				letterFormat.font = proximaNova.fontName;
				letterFormat.size = 28;

				letter.defaultTextFormat = letterFormat;
				letter.text = alphabetString.charAt(i);
				
				letter.filters=[outline];

				item.addChild(letter);
				item.x = _itemPosition + (letterOffset * i);
				item.y = 26;
				item.buttonMode = true;
				item.mouseChildren = false;
				container.addChild(item);
			}
		}
	}
}