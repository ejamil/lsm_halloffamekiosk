﻿/** 
 * Drupal
 * 
 * Singleton: this class is used to call Drupal methods from the Services using AMFPHP
 * Be sure to call Drupal.connect() before calling any Service methods
 * If keys are enabled in Drupal Services, be sure to use "callDrupalSecure" and enter the correct the API Key or DOMAIN constants below
 * If keys are disabled, you can instead used "callDrupal" and don't worry about the API Key or DOMAIN constants
 * All of the public methods allow you to pass in a callback function which will be fired when Drupal has finished saving/loading data.
 *
 * @author			alex@mstudio.com
 * @version			1.0
 * 					
 */


package code 
{
	import flash.net.Responder;	
	import flash.net.NetConnection;
	import flash.net.ObjectEncoding;
	
	import com.adobe.crypto.HMAC;  	// you can also grab these 2 classes at http://code.google.com/p/as3corelib/
	import com.adobe.crypto.SHA256;

	public class Drupal 
	{
		private static var net_connection : NetConnection; 	// this is the object we use to call any Drupal method
		private static var session_id : String; 			// stores our Drupal session id when it is returned from "system.connect"
		private static var responder : Responder; 			// this object calls a success or failure function when Drupal responds
		
		private static const GATEWAY_URL : String 	= "http://fitc.sisutastic.com/services/amfphp"; // grab this from your services page: Home › Administer › Site building › Services -- and click on "AMFPHP - /services/amfphp" -- copy that url and paste it here
		private static const API_KEY: String 		= "YOUR_API_KEY"; 			// if you have keys enabled, you can grab this value in Drupal: Home › Administer › Site building › Services > Keys
		private static const DOMAIN: String 		= "yourdomain.com"; 								// if you have keys enabled, you can grab this value in Drupal: Home › Administer › Site building › Services > Keys
		public static const BASE_PATH: String 		= "http://fitc.sisutastic.com/"; 				// this is used in "Photo.as" as a base path to load images from
		
		private static var callback : Function; // this function is called when Drupal has successfully saved/loaded data -- can be any function you pass in
		
		/**
		 *
		 * 
		 * ---------------------- PUBLIC DRUPAL CALLS --------------------------------
		 *
		 *
		 */		
		
		/**
		 * connect:
		 * 1) AMFPHP gateway and 
		 * 2) Drupal via "system.connect"
		 */
		public static function connect(_callback : Function) : void 
		{
			trace('connecting to Drupal...');
			trace('-------------');
			
			// function to call when connect is successfull
			callback = _callback;
			
			// create an instance of the NetConnection Class
			net_connection = new NetConnection();
			
			// set the encoding of the instance to AMF3
			net_connection.objectEncoding = ObjectEncoding.AMF3;
			
			// connect to the AMFPHP gateway
			net_connection.connect(GATEWAY_URL);
			
			// create a responder to listen for success/error
			responder = new Responder(onConnectSuccess, onDrupalError);
			
			// connect to Drupal
			net_connection.call("system.connect", responder);
		}
		
		/**
		 * callDrupal calls a method from the Drupal Services API
		 * passes in session id and method parameter
		 * @param _method		name of service method to call (from Drupal Services page)
		 * @param _param		parameter to send to Drupal (can be any data type)
		 * @param _callback		function to call when data has successfully loaded/saved
		 */
		public static function callDrupal(_method : String, _param : *, _callback : Function) : void
		{
			trace('calling Drupal: ' + _method);
			trace('-------------');
			
			callback = _callback;
			responder = new Responder(onDrupalSuccess, onDrupalError);
			net_connection.call(_method, responder, session_id, _param);
		}
		
		
		/**
		 * callDrupalSecure calls a method from the Drupal Services API, securely (keys enabled)
		 * passes in hash value, domain, time stamp, nonce, session id and method parameter
		 * @param _method		name of service method to call (from Drupal Services page)
		 * @param _param		parameter to send to Drupal (can be any data type)
		 * @param _callback		function to call when data has successfully loaded/saved
		 */
		public static function callDrupalSecure(_method : String, _param : *, _callback : Function) : void
		{
			trace('calling Drupal w/Keys: ' + _method);
			trace('-------------');
			
			callback = _callback;
			responder = new Responder(onDrupalSuccess, onDrupalError);
			var _key_array : Array = getKeyArray(_method);
			net_connection.call(_method, responder, _key_array[0], _key_array[1], _key_array[2], _key_array[3], session_id, _param);
		}	
		
		
		/**
		 *
		 * 
		 * ---------------------- RESPONDER EVENTS --------------------------------
		 *
		 *
		 */
		

		/**
		 * called when "system.connect" succeeds
		 * saves our session_id, to be used in later calls
		 */
		private static function onConnectSuccess(e : Object) : void 
		{		
			trace('Successfully connected to Drupal.');
			trace('Got session id: ' + e.sessid);
			trace('-------------');
			
			session_id = e.sessid;			
			callback(e);
		}
		
		/**
		 * called when a Drupal method returns a successful response
		 * passes in the Drupal data (e)
		 */
		private static function onDrupalSuccess(e : *) : void 
		{
			trace('Successfully loaded/saved data.');
			trace('-------------');
			
			callback(e);
		}
		
		/*
		 * called if Drupal throws an error
		 * e contains detailed information about the error
		 */
		private static function onDrupalError(e : Object) : void 
		{
			trace('Drupal error: ' + e);
			trace('-------------');
		}
		
		
		/**
		 *
		 * 
		 * ---------------------- UTILITIES FOR SERVICES USING KEYS --------------------------------
		 *
		 *
		 */
		
		
		/*
		 * creates a hash key array based on the service called, the api key, time, nonce and domain
		 */
		public static function getKeyArray(_method:String):Array
		{
			var _time:String = (Math.round((new Date().getTime())/1000)).toString();
			var _nonce:String = getNonce();
			
			// concatenate time, domain, string, nonce, method
			var _message:String = _time + ";";
			_message += DOMAIN + ";";
			_message += _nonce + ";";
			_message += _method;
			
			// creates a string containing the hash value of "_message", using the SHA256 algorithm
			var _hash : String = HMAC.hash(API_KEY,_message,SHA256);
			
			// return the array with 4 parameters to use for the Drupal call
			return [_hash,DOMAIN,_time,_nonce];
		}
		
		/*
		 * creates a 10-character random string to be used in the hash key array
		 */
		private static function getNonce():String
		{
			var _char:String = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			var _char_array:Array = _char.split("");
			var _nonce:String = "";
			for (var i:Number = 0; i < 10; i++)
			{
				_nonce += _char_array[Math.floor(Math.random() * _char_array.length)];
			}
			return _nonce;
		}
		
	
	}
}
