﻿package events
{
	/**
	 * This class holds the event that will be used to initiate sound playback. The reason for this is to centralize the sound playback functionality, so as to avoid the embarassing bug of sound that plays on top of other sound. Included herin are means of specifying whether the sound should interupt, or join a cue.
	 */
	import flash.events.Event;
	
	public class PlaySoundEvent extends Event
	{
		static public const PLAY_SOUND:String = "playSound";
		static public const INTERRUPTING:String = "interrupting";
		static public const ENQUEUE:String = "enqueue";
		static public const PARALLEL:String = "parallel";
		
		public var soundFile:String;
		public var behavior:String;
		
		public function PlaySoundEvent(type:String, bubbles:Boolean, cancelable:Boolean, _soundFile:String, _behavior:String = PlaySoundEvent.INTERRUPTING):void
		{
			super(type, bubbles, cancelable);
			soundFile = _soundFile;
			behavior = _behavior;
		}	
	}
}