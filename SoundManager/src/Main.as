package 
{
	import events.PlaySoundEvent;
	import flash.display.Sprite;
	import flash.events.Event;
	import media.SoundManager;
	
	/**
	 * ...
	 * @author Chris
	 */
	public class Main extends Sprite 
	{
		var soundManager:SoundManager;
		
		public function Main():void 
		{
			soundManager = new SoundManager();
			
			var firstSoundEvent:PlaySoundEvent = new PlaySoundEvent(PlaySoundEvent.PLAY_SOUND, false, false, "Start_Button.mp3");
			var secondSoundEvent:PlaySoundEvent = new PlaySoundEvent(PlaySoundEvent.PLAY_SOUND, false, false, "Wrong_Item.mp3", PlaySoundEvent.ENQUEUE);
			
			soundManager.load(firstSoundEvent);
			soundManager.load(secondSoundEvent);
		}
		
		
		
	}
	
}