package media 
{
	import events.PlaySoundEvent;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.media.Sound;
	/**
	 * ...
	 * @author Chris Okerberg
	 */
	public class SoundManager extends EventDispatcher
	{
		private var soundPlayer:SoundPlayer;
		private var soundList:Vector.<String>;
		private var sfxPlayer:SoundPlayer;
		private var gameLoopPlayer:SoundPlayer;
		
		public function SoundManager() 
		{
			soundPlayer = new SoundPlayer;
			sfxPlayer = new SoundPlayer;
			sfxPlayer.limit = 20;
			gameLoopPlayer = new SoundPlayer;
			gameLoopPlayer.limit = 20;
			soundList = new Vector.<String>;
		}
		
		public function PlayLoop(loop:String):void 
		{
			gameLoopPlayer.load(loop);
			gameLoopPlayer.loopSound = true;
		}
		
		public function StopLoop():void
		{
			gameLoopPlayer.stop();
		}
		
		public function load(e:PlaySoundEvent):void 
		{
			switch (e.behavior)
			{
				case PlaySoundEvent.INTERRUPTING:
					soundPlayer.stop();
					sfxPlayer.stop();
					soundList = new Vector.<String>;
					soundPlayer.load(e.soundFile);
					soundPlayer.addEventListener(SoundPlayer.COMPLETE, soundCompleteHandler, false, 0, true);
					break;
				case PlaySoundEvent.ENQUEUE:
					soundList.push(e.soundFile);
					break;
				case PlaySoundEvent.PARALLEL:
					//// ////////trace("parallel requested");
					//var newSound = new SoundPlayer;
					//newSound.load(e.soundFile);
					sfxPlayer.load(e.soundFile);
			}
		}
		
		private function soundCompleteHandler(e:Event=null):void 
		{
			if (soundList.length)
			{
				soundPlayer.load(soundList.shift());
			} else {
				soundPlayer.removeEventListener(SoundPlayer.COMPLETE, soundCompleteHandler);
				dispatchEvent(new Event(SoundPlayer.COMPLETE));
			}
		}
		
	}

}