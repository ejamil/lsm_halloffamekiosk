package media 
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.media.SoundMixer;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	import flash.net.URLRequest;
	/**
	 * ...
	 * @author Chris Okerberg
	 */
	public class SoundPlayer extends EventDispatcher
	{
		public static const COMPLETE:String = "complete";
		private var _loopSound:Boolean;
		private var sound:Sound;
		private var soundURL:URLRequest;
		private var soundChannel:SoundChannel;
		private var _limit:Number;
		private var trans:SoundTransform;
		private var _url:String;
		
		public function SoundPlayer() 
		{
			_limit = 1;
			_loopSound = false;
		}
		
		public function set limit(value:Number):void
		{
			_limit = value / 100;
		}
		
		public function set loopSound(value:Boolean):void 
		{
			_loopSound = value;
		}
		public function load(url:String) {
			_url = url;
			soundURL = new URLRequest("assets/sounds/" + url);
			trans = new SoundTransform(_limit);
			sound = new Sound(soundURL);
			soundChannel = sound.play(0,1,trans);
			soundChannel.addEventListener(Event.SOUND_COMPLETE, soundCompleteHandler, false, 0, true);
		}
		
		public function stop():void 
		{
			if (soundChannel) soundChannel.stop();
		}
		
		public function play():void 
		{
			soundChannel = sound.play(0,1,trans);
			soundChannel.addEventListener(Event.SOUND_COMPLETE, soundCompleteHandler, false, 0, true);
		}
		
		private function soundCompleteHandler(e:Event):void 
		{
			soundChannel.removeEventListener(Event.SOUND_COMPLETE, soundCompleteHandler);
			
			
			if (_loopSound) 
				load(_url);
			else 
				dispatchEvent(new Event(SoundPlayer.COMPLETE));
		}
		
		public function close():void 
		{
			soundChannel.removeEventListener(Event.SOUND_COMPLETE, soundCompleteHandler);
			soundChannel.stop();
		}
		
	}

}